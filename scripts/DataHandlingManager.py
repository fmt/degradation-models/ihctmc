from aux_functions import load_network_data, load_inspection_data, getFrequencyTable
import pandas as pd
from copy import copy

class DataHandlingManager:
    def __init__(self,**kwargs):
        self.system = self.getSystemData(path=kwargs.get('system_database',''))
        self.inspections = self.getInspectionData(path=kwargs.get('inspection_database',''))

    def getSystemData(self, path):
        """
        Loads system network data from a specified database path.
    
        Args:
        - path (str): The file path to the database.
    
        Returns:
        - DataFrame: A DataFrame containing the loaded system network data.
        """
        return load_network_data(db_path=path)
    
    def getInspectionData(self, path):
        """
        Loads inspection data from a specified database path, filtering by the system network.
    
        Args:
        - path (str): The file path to the database.
    
        Returns:
        - DataFrame: A DataFrame containing the loaded inspection data filtered by the system network.
        """
        return load_inspection_data(db_path=path, network=self.system)
    
    def get_lifetime_data(self, code, cohort, flag='most_critical', add_collapse=False, cut_max_age=None, cut_max_age_flag='low', location=None):
        """
        Retrieves lifetime data for pipes within a specified cohort, filtering and processing the data based on various criteria.
    
        Args:
        - code (str): The code used to identify specific data.
        - cohort (int): The cohort number to filter pipes by.
        - severities (list, optional): A list of severities to consider. Defaults to [1,2,3,4,5,6].
        - flag (str, optional): Criteria to filter the data ('most_critical' or 'over_length'). Defaults to 'most_critical'.
        - add_collapse (bool, optional): Whether to add collapse data. Defaults to False.
        - cut_max_age (float, optional): The maximum age to filter pipes by. None if not filtering by age.
        - cut_max_age_flag (str, optional): Specifies whether to filter by ages lower than or higher than `cut_max_age` ('low' or 'up'). Defaults to 'low'.
        - location (str, optional): The location to filter pipes by. None if not filtering by location.
    
        Returns:
        - DataFrame: A DataFrame containing processed pipe data, filtered and sorted according to the specified parameters.
        """
        # Selecting the relevant pipes within the cohort from the system
        cohort_pipes = self.system[self.system['cohort'] == cohort]['pipe_id']
        df = self.inspections[self.inspections['pipe_id'].isin(cohort_pipes)]
        
        # Merging with system DataFrame to get additional attributes
        df = df.merge(self.system[['pipe_id', 'construction_year', 'cohort', 'length', 'width']], on='pipe_id')
        
        # Converting dates to datetime objects and calculating pipe_age
        df['inspection_date'] = pd.to_datetime(df['inspection_date'])
        df['construction_year'] = pd.to_datetime(df['construction_year'].astype(int).astype(str) + '-01-01')
        df['pipe_age'] = (df['inspection_date'] - df['construction_year']).dt.total_seconds() / (365.25 * 24 * 60 * 60)
        
        # Keeping only non-negative pipe ages
        df = df[df['pipe_age'] >= 0]
        
        # Filter by cut max age
        if cut_max_age:
            if cut_max_age_flag == 'low':
                df = df[df['pipe_age'] <= cut_max_age]
            elif cut_max_age_flag == 'up':
                df = df[df['pipe_age'] > cut_max_age]
    
        df2 = pd.DataFrame()
        if len(df) > 0:
            # Filter by location
            if location:
                df = df[df['location'] == location]
            # Converting codes not matching the input code to level 1
            non_matching_codes = df['code'] != code
            df.loc[non_matching_codes, 'code'] = code
            df.loc[non_matching_codes, 'damage_class'] = 1
            # Add failure data
            if add_collapse:
                df.loc[self.inspections['code'] == 'BAC', 'damage_class'] = 6
            if flag == 'most_critical':
                # Identifying the most critical severity for each inspection
                grouped_inspection = df.groupby('inspection_id')
                df2 = grouped_inspection.agg({
                    'pipe_id': 'first',
                    'pipe_age': 'first',
                    'damage_class': 'max'
                }).reset_index()
                df2.rename(columns={'damage_class': 'severity'}, inplace=True)
            elif flag == 'over_length':
                df2 = df[['pipe_id', 'pipe_age', 'damage_class']]
                df2.rename(columns={'damage_class': 'severity'}, inplace=True)
            df2 = df2.sort_values(by='pipe_age', ascending=True).reset_index(drop=True)
            
        # Change format for higher compatibility:
        df2.rename(columns={'pipe_age': 'time', 'severity': 'state'}, inplace=True)
        df2.drop(['inspection_id', 'pipe_id'], axis=1, inplace=True)
        df2 = df2.sort_values(by='time', ascending=True).reset_index(drop=True)
        df2 = df2.groupby(['time', 'state']).size().reset_index(name='count')
        df2.set_index('time', inplace=True)
        
        # Change state 6 for "F"
        df2.loc[df2['state'] == 6, 'state'] = 'F'

        return df2


if __name__ == "__main__":
    args = {
        'system_database':'/Users/lisandro/Library/CloudStorage/OneDrive-UniversiteitTwente/PhD/Papers/datasets/raw/breda.db',
        'inspection_database':'/Users/lisandro/Library/CloudStorage/OneDrive-UniversiteitTwente/PhD/Papers/datasets/raw/breda.db',
        } 
    dh = DataHandlingManager(**args)
    df = dh.get_lifetime_data(code='BAF',cohort='CMW',add_collapse=True)
    ft = getFrequencyTable(y=copy(df),states=[1,2,3,4,5,'F'],delta=3)
    