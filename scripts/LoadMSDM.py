import sys
import os

# Add the 'scripts' directory and its subdirectories to sys.path
folder_path = os.path.join(os.path.dirname(os.path.dirname(os.getcwd())), 'code', 'files', 'scripts')
sys.path.append(folder_path)
for root, dirs, files in os.walk(folder_path):
    for dir in dirs:
        sys.path.append(os.path.join(root, dir))

from ihtmc import InhomogeneousTimeMarkovChain as IHTMC
import numpy as np

class mapMSDM:
    def __init__(self):
        """Initialize the mapMSDM object by loading the Markov State-Dependent Models (MSDM)."""
        self.msdm = self.load_msdm()
    
    def get(self, cov, function='gompertz', damage='BAF'):
        """
        Retrieve a specific value from the MSDM based on the provided parameters.
        
        Parameters:
        - cov (pd.Series): A pandas Series containing the covariates for the query.
        - function (str, optional): The hazard function to use. Defaults to 'gompertz'.
        - damage (str, optional): The damage model to use. Defaults to 'BAF'.
        
        Returns:
        - The value from the MSDM matching the specified parameters.
        """
        _vars = list(set(self.all_var_msdm) & set(list(cov[cov.values==1].index)))
        f = _vars + [function] + [damage]
        return self.get_value_from_msdm(self.msdm, f)
    
    def load_msdm(self, MCStructureID='s6_typeA'):
        """
        Load the MSDM based on the specified MCStructureID and calibrate the degradation models.
        
        Parameters:
        - MCStructureID (str, optional): The ID of the Markov Chain structure to use. Defaults to 's6_typeA'.
        
        Returns:
        - A dictionary containing the calibrated MSDM models.
        """
        if MCStructureID == 's5_typeA':
            out = {}
            
            # Inhomogeneous
            msdm          =  IHTMC(MCStructureID=MCStructureID,hazard_function='gompertz')
            msdm.MCObj.s0 = np.array([0.9,0.05,0.025,0.025,0.0])
            msdm.MCObj.x  = np.array([0.1,0.1,0.1,0.1,0.045,0.05,0.05,0.03])
            out[('ground_truth_inhomogeneous','ground_truth_gompertz')] = msdm 
            
            msdm          =  IHTMC(MCStructureID=MCStructureID,hazard_function='gompertz')
            msdm.MCObj.s0 = np.array([0.9,0.05,0.025,0.025,0.0])
            msdm.MCObj.x  = np.array([0.1,0.1,0.1,0.1,0.045,0.05,0.05,0.03])
            out[('ground_truth_inhomogeneous','gompertz')] = msdm 
            
            msdm          =  IHTMC(MCStructureID=MCStructureID,hazard_function='exponential')
            msdm.MCObj.s0 = np.array([9.99142301e-01, 2.79993267e-05, 2.89711487e-04, 1.72198238e-04,3.67790408e-04])
            msdm.MCObj.x  = np.array([0.02270134, 0.05275196, 0.07244865, 0.01719452])
            out[('ground_truth_inhomogeneous','exponential')] = msdm 
            
            
            # Homogeneous
            
            msdm          =  IHTMC(MCStructureID=MCStructureID,hazard_function='exponential')
            msdm.MCObj.s0 = np.array([9.99142301e-01, 2.79993267e-05, 2.89711487e-04, 1.72198238e-04,3.67790408e-04])
            msdm.MCObj.x  = np.array([0.02270134, 0.05275196, 0.07244865, 0.01719452])
            out[('ground_truth_homogeneous','ground_truth_exponential')] = msdm 
            
            msdm          =  IHTMC(MCStructureID=MCStructureID,hazard_function='exponential')
            msdm.MCObj.s0 = np.array([9.99142301e-01, 2.79993267e-05, 2.89711487e-04, 1.72198238e-04,3.67790408e-04])
            msdm.MCObj.x  = np.array([0.02270134, 0.05275196, 0.07244865, 0.01719452])
            out[('ground_truth_homogeneous','exponential')] = msdm 
            
            msdm          =  IHTMC(MCStructureID=MCStructureID,hazard_function='gompertz')
            msdm.MCObj.s0 = np.array([0.73079851, 0.09996795, 0.08183643, 0.07216788, 0.01522922])
            msdm.MCObj.x  = np.array([0.19999992, 0.19999994, 0.19999994, 0.19999748, 0.02755406,0.0356125 , 0.03996462, 0.02231694])
            out[('ground_truth_homogeneous','gompertz')] = msdm 
            
        elif MCStructureID == 's6_typeA':
            out = {}
    
            #%% Cohort: CMW, Damage: BAF 
            # Assign parameters for: cohort: CMW, damage: BAF
            msdm  =  IHTMC(MCStructureID=MCStructureID,hazard_function='gompertz')
            # Assign parameters for: function: gompertz, metric: RMSE
            msdm.MCObj.x  = np.array([2.29871662e+00, 2.09331310e-02, 3.28824979e+00, 2.43903545e+00, 1.44257128e-01, 8.75057330e-01, 2.16499018e-03, 9.78052040e-05, 7.00000000e-19, 8.36645098e-03, 5.47218847e-02, 2.75333617e-03, 8.71054881e-03, 3.05224181e-04, 7.00000000e-19, 4.53206876e-02, 8.56625756e-03, 3.80129954e-01])
            msdm.MCObj.s0 = np.array([9.58238193e-01, 0.00000000e+00, 3.99999361e-02, 1.60545379e-03, 1.99950027e-15, 1.56417474e-04])
            out[('pipe_material_concrete','pipe_content_mixed','BAF','gompertz')] = msdm 
            
            msdm  =  IHTMC(MCStructureID=MCStructureID,hazard_function='weibull')
            # Assign parameters for: function: gompertz, metric: RMSE
            msdm.MCObj.x  = np.array([1.26687091e+00, 2.87201477e+00, 3.46465084e+00, 6.95539086e+00, 4.08067348e-06, 2.71502543e-04, 3.04655849e-05, 1.09879096e-03, 1.66215483e+00, 4.41776330e+01, 7.72776870e+01, 8.08004780e+01, 5.53672251e+01, 4.61327918e+01, 4.57428363e+01, 4.71166290e+01, 4.50557449e+01, 5.91222452e+01])
            msdm.MCObj.s0 = np.array([0.92330697, 0.02593101, 0.03103027, 0.01126157, 0.0020724, 0.00639777])
            out[('pipe_material_concrete','pipe_content_mixed','BAF','weibull')] = msdm 
    
            msdm  =  IHTMC(MCStructureID=MCStructureID,hazard_function='exponential')
            # Assign parameters for: function: exponential, metric: RMSE
            msdm.MCObj.x  = np.array([2.44222779e-02, 9.38162130e-03, 5.67550494e-03, 1.83371198e-02, 3.04589829e-18, 6.02589204e-04, 1.00003674e-18, 1.00000824e-18, 1.00003081e-18])
            msdm.MCObj.s0 = np.array([9.88862546e-01, 1.25750483e-17, 3.70336428e-23, 1.11374538e-02, 2.10613586e-22, 3.86513331e-22])
            out[('pipe_material_concrete','pipe_content_mixed','BAF','exponential')] = msdm
            #%% Cohort: CR, Damage: BAF 
            msdm  =  IHTMC(MCStructureID=MCStructureID,hazard_function='gompertz')
            # Assign parameters for: function: gompertz, metric: RMSE
            msdm.MCObj.x  = np.array([8.12782198e-02, 6.92950891e-02, 2.18411264e-01, 1.45444996e-01, 2.24537010e-01, 5.95783710e-03, 4.02231256e-02, 1.58318744e-01, 1.43633161e-01, 5.26089404e-02, 3.96397124e-02, 4.13113049e-02, 2.12217527e-01, 1.73123461e-05, 9.64901834e-03, 3.35072421e-02, 2.06496818e-02, 4.72360452e-04])
            msdm.MCObj.s0 = np.array([9.76625745e-01, 1.67209289e-02, 1.25483346e-05, 3.99253580e-04, 2.41880831e-03, 3.82271566e-03])
            out[('pipe_material_concrete','pipe_content_stormwater','BAF','gompertz')] = msdm 
            
            msdm  =  IHTMC(MCStructureID=MCStructureID,hazard_function='weibull')
            # Assign parameters for: function: gompertz, metric: RMSE
            msdm.MCObj.x  = np.array([  3.24692391,   2.78633401,   2.46314674,   1.03437623, 13.68242486,   6.73305894,  22.01786267, 0.7748914, 0.19312462,  47.03892339,  66.17257989,  33.21624627, 1.92669482, 149.72616303, 148.96230302, 122.91538727, 75.53001786,  72.52928181])
            msdm.MCObj.s0 = np.array([9.40918405e-01, 5.28612383e-02, 1.53416143e-04, 5.06635619e-06, 3.82136563e-03, 2.24050812e-03])
            out[('pipe_material_concrete','pipe_content_stormwater','BAF','weibull')] = msdm 
            
            msdm  =  IHTMC(MCStructureID=MCStructureID,hazard_function='exponential')
            # Assign parameters for: function: exponential, metric: RMSE
            msdm.MCObj.x  = np.array([1.36977365e-02, 8.72342669e-03, 1.82167553e-01, 9.71701775e-02, 1.00000000e-12, 1.00000000e-12, 1.00000000e-12, 1.28705786e-01, 0.00000000e+00])
            msdm.MCObj.s0 = np.array([9.96508429e-01, 1.57138494e-04, 1.28980458e-03, 1.00002629e-04, 1.84462217e-03, 1.00002629e-04])
            out[('pipe_material_concrete','pipe_content_stormwater','BAF','exponential')] = msdm 
            #%% Cohort: PMW, Damage: BAF 
            
            # All variables in the MSDM
            keys = list(out.keys())
            self.all_var_msdm = list(set(value for tuple_ in keys for value in tuple_))
        return out
    
    def get_value_from_msdm(self, msdm, query):
        """
        Retrieve a value from the MSDM based on a query.
        
        Parameters:
        - msdm (dict): The MSDM dictionary.
        - query (list): The query list containing parameters for the MSDM lookup.
        
        Returns:
        - The value associated with the query in the MSDM.
        
        Raises:
        - KeyError: If no matching key is found for the query.
        """
        query_set = set(query)
        for key in msdm.keys():
            key_set = set(key)
            if key_set == query_set:
                return msdm[key]
        raise KeyError(f"No matching key found for query: {query}")

    
    