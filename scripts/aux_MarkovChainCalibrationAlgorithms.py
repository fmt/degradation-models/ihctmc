import numpy as np

def sum_constraint(x, N):
    """
    Constraint function to ensure the sum of the last N elements of x is 1.

    Parameters:
    - x: np.ndarray, the array of optimization variables.
    - N: int, the number of elements from the end of x to sum.

    Returns:
    - float, the difference between the sum of the last N elements and 1.
    """
    return np.sum(x[-N:]) - 1


def SLSQP(obj, **args):
    """
    Optimizes an objective function using the Sequential Least Squares Programming (SLSQP) method.

    Parameters:
    obj (object): An object containing the cost function and additional optimization settings.
    args (dict): A dictionary containing the optimization parameters. Expected keys are:
                 'metric' (str): The metric for measuring errors. Default is 'LogL'.
                 'maxiter' (int): The maximum number of iterations. Default is 1000.
                 'verbose' (int): The verbosity level. Default is 0.
                 'tol' (float): The tolerance for termination. Default is 1e-10.

    Returns:
    result: The result of the optimization process.
    """
    # Extracting optimization parameters with defaults
    metric = args.get('metric', 'LogL')
    maxiter = args.get('maxiter', 1000)
    verbose = args.get('verbose', 0)
    tol = args.get('tol', 1e-10)  # Corrected key from 'verbose' to 'tol'

    from scipy.optimize import minimize

    # Performing the optimization
    result = minimize(obj.cost_function,
                      obj._.MCObj.getXFromMCParameters(),
                      args=(metric,),
                      method='SLSQP',
                      bounds=obj._.MCObj.bounds,
                      tol=tol,
                      options={'disp': verbose > 0, 'maxiter': maxiter}
                      )
    return result


def trust_constr(obj,**args):
    """
    Optimizes an objective function using the Trust Region Constrained (trust-constr) optimization method.

    Parameters:
    obj (object): An object containing the cost function and additional optimization settings.
    args (dict): A dictionary containing the optimization parameters. Expected keys are:
                 'metric' (str): The metric for measuring errors. Default is 'LogL'.
                 'maxiter' (int): The maximum number of iterations. Default is 1000.
                 'verbose' (int): The verbosity level. Default is 0.

    Returns:
    result: The result of the optimization process.
    """
    # Extracting optimization parameters with defaults
    metric = args.get('metric', 'LogL')
    gtol = args.get('gtol', 1e-10)
    maxiter = args.get('maxiter', 1000)
    verbose = args.get('verbose', 0)
    xtol = args.get('xtol', 1e-10)
    finite_diff_rel_step = args.get('finite_diff_rel_step', 0.001)
    
    from scipy.optimize import minimize

    # Performing the optimization
    result = minimize(obj.cost_function,  
                      obj._.MCObj.getXFromMCParameters(),
                      args=(metric,),
                      method='trust-constr', 
                      bounds=obj._.MCObj.bounds,
                      options={'finite_diff_rel_step': finite_diff_rel_step,
                               'maxiter': maxiter,
                               'verbose': verbose,
                               'xtol': xtol,
                               'gtol': gtol,
                               }
                      )
    return result


def Nelder_Mead(obj, **args):
    """
    Optimizes an objective function using the Nelder-Mead simplex algorithm.

    Parameters:
    obj (object): An object containing the cost function and additional optimization settings.
    args (dict): A dictionary containing the optimization parameters. Expected keys are:
                 'metric' (str): The metric for measuring errors. Default is 'LogL'.
                 'maxiter' (int): The maximum number of iterations. Default is 1000.

    Returns:
    result: The result of the optimization process.
    """
    metric = args.get('metric', 'LogL')
    maxiter = args.get('maxiter', 1000)
    
    from scipy.optimize import minimize

    result = minimize(obj.cost_function, 
                      obj._.MCObj.getXFromMCParameters(),
                      args=(metric,),
                      method='Nelder-Mead', 
                      options={'xatol': 1e-8,  # Tolerance for termination in the x direction
                               'fatol': 1e-8,  # Tolerance for termination in the function direction
                               'disp': True,  # Display the convergence messages
                               'maxiter': maxiter})  # Maximum number of iterations
    return result

def differential_evolution(obj, **args):
    """
    Optimizes an objective function using the differential evolution global optimization algorithm.

    Parameters:
    obj (object): An object containing the cost function and additional optimization settings.
    args (dict): A dictionary containing the optimization parameters. Expected keys are:
                 'disp' (bool): Display status messages. Default is True.
                 'popsize' (int): Population size. Default is 20.
                 'polish' (bool): Whether to perform a local minimization at the end. Default is False.
                 'workers' (int): Number of workers for parallel computation. Default is 1.
                 'updating' (str): Whether to update candidates immediately or after all evaluations. Default is 'immediate'.
                 Other keys like 'metric' and 'maxiter' should be defined outside this function or passed explicitly if needed.

    Returns:
    result: The result of the optimization process.
    """
    from scipy.optimize import differential_evolution#, NonlinearConstraint

    # Retrieving optimization parameters from args with defaults
    disp = args.get('disp', True)
    popsize = args.get('popsize', 20)
    polish = args.get('polish', False)
    workers = args.get('workers', 1)
    maxiter = args.get('maxiter', 1000)  # Assuming 'maxiter' is intended to be part of args with a default value
    metric = args.get('metric', 'LogL')  # Assuming 'metric' needs to be included
    tol = args.get('tol', 0.001)
    
    # Setting 'updating' strategy based on the number of workers
    updating = 'immediate' if workers == 1 else 'deferred'

    result = differential_evolution(obj.cost_function, 
                                    obj._.MCObj.bounds, 
                                    args=(metric,),
                                    strategy='best1bin',  # Strategy for generating trial candidates
                                    maxiter=maxiter,  # Maximum number of generations over which to evolve population
                                    popsize=popsize,  # Population size
                                    tol=tol,  # Relative tolerance for convergence
                                    mutation=(0.5, 1),  # Mutation constant or a tuple specifying mutation range
                                    recombination=0.7,  # Recombination constant
                                    seed=None,  # Seed for random number generator
                                    disp=disp,  # Display status messages
                                    callback=None,  # A callback function for additional functionality at each iteration
                                    polish=polish,  # Whether to perform a local minimization at the end
                                    init='latinhypercube',  # Initialization method ('latinhypercube', 'random', or an array)
                                    atol=0,  # Absolute tolerance for convergence
                                    updating=updating,  # Whether to update candidates immediately or after all evaluations
                                    #constraints=(NonlinearConstraint(lambda x: sum_constraint(x, len(obj._.MCObj.states)), 0, 0),)  # Constraints definition.
                                    workers=workers)
    return result

# TODO: Baum-Welch / Expectation Maximmization algorithms
# TODO: MEtropoli-Hastings algorithm

# def metropolis_hastings(self,pt,x0,model_type,function,func_obj,iterations=1000, burn_in=500,output_convergence=False,norm_std_deviation=0.1):
#     """
#     Metropolis-Hastings algorithm to estimate the transition probabilities of an inhomogeneous Markov chain.
#     :param pt: dataframe with two columns [time, state]
#     :param x0: Markov chain initial parameters
#     :param iterations: Number of iterations for the MCMC
#     :param burn_in: Number of iterations to discard for burn-in
#     :return: Estimated transition matrix
#     """
    
#     def calculate_likelihood(data,params,P0,model_type,function):
#         if model_type == 'dtmc':
#             transition_matrix = func_obj.Q_matrix(params)
#             initial_vector = list(P0)
#             steps = data['pipe_age']
#             p = func_obj.unroll(transition_matrix,np.array(initial_vector),list(steps))
#             return self.log_likelihood(p,data)
#         elif model_type == 'nhtmc':
#             p,s = func_obj.unroll(t=data['pipe_age'],params=params,P0=P0,function=function)
#             if s:
#                 # The solver succeeded
#                 return self.log_likelihood(p,data)
#             else:
#                 # The solver failed
#                 return -1E100
#             #return self.log_likelihood(p,data)
        
    
#     # Perform Metropolis-Hastings sampling
#     current_x0 = x0[0:-6]
#     current_P0 = x0[-6:]
    
#     all_log_likelihoods = [calculate_likelihood(data=pt,
#                                               params=current_x0,
#                                               P0=current_P0,
#                                               model_type = model_type,
#                                               function = function,
#                                               )]
    
#     all_x0 = [current_x0]
#     all_P0 = [current_P0]

#     for _ in range(iterations):
#         # Propose a new sample by perturbing the current sample
#         proposed_x0 = current_x0 + norm.rvs(scale=norm_std_deviation,size=current_x0.shape)
        
#         if model_type == 'dtmc':
#             proposed_x0[proposed_x0>1] = 1
#             proposed_x0[proposed_x0<0] = 0
#         else:
#             proposed_x0[proposed_x0<0] = 1E-12 # Ensure non-negative coefficients
        
#         proposed_P0 = self.add_noise_to_pmf(current_P0,noise_scale=0.001)#0.001 --> We are not modifying this vector with the M-H algorithm

#         # Calculate the acceptance ratio
#         current_log_pdf    = calculate_likelihood(data=pt,
#                                                   params=current_x0,
#                                                   P0=current_P0,
#                                                   model_type=model_type,
#                                                   function=function,
#                                                   )
#         candidate_log_pdf = calculate_likelihood(data=pt,
#                                                  params=proposed_x0,
#                                                  P0=proposed_P0,
#                                                  model_type=model_type,
#                                                  function=function,
#                                                  )
#         log_acceptance_prob = candidate_log_pdf - current_log_pdf

#         # Accept or reject the candidate sample based on the log of the acceptance probability
#         if np.log(np.random.rand()) < log_acceptance_prob:
#             print([_, candidate_log_pdf])
#             current_x0 = proposed_x0
#             current_P0 = proposed_P0
#             all_log_likelihoods.append(candidate_log_pdf)
#         else:
#             all_log_likelihoods.append(current_log_pdf)
#         all_x0.append(current_x0)
#         all_P0.append(current_P0)

#     convergence_df = pd.DataFrame()
#     if output_convergence:
#         convergence_df = pd.DataFrame({'x0': all_x0,
#                                        'P0': all_P0,
#                                        'log-likelihood': all_log_likelihoods}
#                                       )

#     # Output parameters:
#     results = {'x':list(np.array(all_x0[burn_in:]).mean(axis=0))+list(np.array(all_P0[burn_in:]).mean(axis=0)),
#                'convergence':convergence_df}

#     return results

