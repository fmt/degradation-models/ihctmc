import pandas as pd
import numpy as np
from aux_functions import renormalize_mass_function, comp_error, getFrequencyTable
from markov_chain_calibration import MarkovChainCalibration

def compute_error(_self, y=pd.DataFrame(), yp=pd.DataFrame(), metric='RMSE'):
    """
    Compare the error between actual and predicted dataframes based on a specified metric.

    Parameters:
    - self: Instance of the class containing this function, the original dataframe (self.df), and the Markov Chain object (self.MCObj).
    - y (pd.DataFrame, optional): Actual data. If not provided, the instance's dataframe (self.df) is used.
    - yp (pd.DataFrame, optional): Predicted data. If not provided, predictions are made using the instance's predict method on the actual data's index.
    - metric (str, optional): The metric to use for comparison. Default is 'LogL' (Log Likelihood).

    Returns:
    - The result of the comparison using the specified metric, computed by the comp_error function.
    """            

    if yp.empty:
        yp, success = _self.predict(t=_self.df.index.unique(),ConverenceDetails=True)
    
    if success:
        if metric == 'LogL':
            if y.empty:
                y = _self.df
        elif metric in ['RMSE','SE','MSE']:
            if _self.df_freq.empty:
                _self.df_freq = getFrequencyTable(y=_self.df,states=_self.MCObj.states)
            y = _self.df_freq 
                
        return comp_error(y=y, yp=yp, metric=metric, states=_self.MCObj.states)
    else:
        return None
    

def renormalize_rows(dataframe, tolerance=0.01):
    """
    Renormalizes the rows of a dataframe. Rows containing any negative value are set to 0 and then normalized 
    so that each row sums exactly to 1. Rows are adjusted to sum to 1 within a specified tolerance.

    Parameters:
    - dataframe (pd.DataFrame): The dataframe to be renormalized.
    - tolerance (float): The tolerance within which the sum of rows must be to 1.

    Returns:
    - pd.DataFrame: The renormalized dataframe.
    """
    # Set negative values to 0 and normalize rows
    dataframe[dataframe < 0] = 0
    row_sums = dataframe.sum(axis=1)
    dataframe = dataframe.div(row_sums, axis=0).fillna(0)
    
    # Adjust rows to ensure sum is within tolerance to 1
    row_sums_after = dataframe.sum(axis=1)
    adjust_mask = np.abs(row_sums_after - 1) > tolerance
    dataframe.loc[adjust_mask] = dataframe.loc[adjust_mask].div(row_sums_after[adjust_mask], axis=0)
    
    return dataframe


def sample(_self, t=np.linspace(0, 50), n=1000, states=[], exact_t_spacing=False):
    """
    Samples state observations from a Markov Chain model over a specified time interval.

    Parameters:
    - _self: The object instance.
    - t (array-like, optional): The time points for prediction. Defaults to a linear space between 0 and 50.
    - n (int, optional): The number of observations to sample. Defaults to 1000.
    - states (list, optional): The states to sample from. Defaults to all states in the Markov Chain model.
    - exact_t_spacing (bool, optional): If True, samples are taken at exact times from t. If False, samples are
      taken at uniform random times between min(t) and max(t). Defaults to False.

    Returns:
    - A DataFrame containing the sampled observations, with columns for time, state, and count of each state
      at each sampled time point. The DataFrame is indexed by time and sorted in ascending order.
    """
    g = _self.predict(t=t)
    if states == []:
        states = _self.MCObj.states
    if exact_t_spacing:
        times = np.random.choice(t, n)
    else:
        times = np.random.uniform(min(t), max(t), n)
    choices = [np.random.choice(states, p=g.iloc[np.argmin(np.abs(ti - t))].values) for ti in times]
    obs = np.column_stack((times, choices))
    df = pd.DataFrame(obs, columns=['time', 'state']).sort_values(by='time', ascending=True).reset_index(drop=True)
    df = df.groupby(['time', 'state']).size().reset_index(name='count')
    df.set_index('time', inplace=True)
    return df

def fit(_self,args):
    """
    Fits the Markov Chain model to the provided data, calibrating its parameters.
    
    Parameters:
    - df (pd.DataFrame, optional): DataFrame containing the data for calibration. Default is an empty DataFrame.
    - metric (str, optional): The metric to optimize during calibration. Default is 'LogL'.
    - severities (list of int, optional): List of severity states to consider in the model. Default is [1,2,3,4,5,6].
    - markov_chain_type (str, optional): Name of the Markov Chain type. Default is 'Type A'.
    
    Returns:
    - MarkovChainStructure: The calibrated Markov Chain Structure object with updated parameters.
    """
    # Get args
    df = args.get('df',pd.DataFrame())
    metric = args.get('metric','RMSE')

    # Get data:
    df = _self.fetch_data(df)
    # Obtain model parameters:
    model = MarkovChainCalibration(df,_self,metric=metric)
    result = model.fit(**args)
    # Update model parameters
    _self.MCObj.calibrated = True
    param = _self.MCObj.getMCParametersFromX(result.x)
    _self.MCObj.x, _self.MCObj.s0 = param['x'], param['s0']
    _self.MCObj.convergence_info = result
    return _self.MCObj