#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 21:01:20 2024

@author: lisandro
"""
import os
import numpy as np
import pandas as pd
import pickle
import sqlite3
import re
from datetime import datetime
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.base import BaseEstimator, TransformerMixin
import gymnasium
from gymnasium import spaces
import string
import random
from collections import Counter
import datetime
from copy import deepcopy
from scipy.stats import mode
from typing import Callable, Dict, Optional
import dill
import matplotlib.pyplot as plt
import importlib
from scipy.interpolate import interp1d
import logging


def extract_wkt_coords(df, wkt_column):
    pattern = r'(\d+(?:\.\d+)?)\s+(\d+(?:\.\d+)?)'
    coords_array = []
    for wkt in df[wkt_column].astype(str):
        matches = re.findall(pattern, wkt)
        row_coords = [float(num) for coords in matches for num in coords] if matches else [np.nan] * 4
        coords_array.append(row_coords)
    return coords_array

def normalize_coordinates(df):
    x_min = min(df['x_i'].min(), df['x_f'].min())
    x_max = max(df['x_i'].max(), df['x_f'].max())
    y_min = min(df['y_i'].min(), df['y_f'].min())
    y_max = max(df['y_i'].max(), df['y_f'].max())

    # Normalized to [-1, 1]
    df['x_i'] = (df['x_i'] - x_min) / (x_max - x_min)
    df['x_f'] = (df['x_f'] - x_min) / (x_max - x_min)
    df['y_i'] = (df['y_i'] - y_min) / (y_max - y_min)
    df['y_f'] = (df['y_f'] - y_min) / (y_max - y_min)
    return df

def fetch_table_contents(db_path,table_name):
    with sqlite3.connect(db_path) as conn:
        df = pd.read_sql_query(f"SELECT * FROM {table_name}", conn)
    return df 

def load_network_data(db_path=None,df_path=None,table_name='sewer_pipes', apply_filters=True, sub_network=True, build_cohorts=True,norm_coordinates=False,system_features_var_blacklist=None):
            
    if db_path:
        df = fetch_table_contents(db_path,table_name)
        df = df.drop(columns=['start_node_id', 'end_node_id', 'material_org', 'start_floorlevel', 'end_floorlevel', 'pipeshape', 'height', 'customer_connections', 'effective_customer_connections', 'geometry'])
    elif df_path:
        '''
        Read dataframe.
        '''
        with open(df_path, 'rb') as file:
            # Return the loaded object
            df = pickle.load(file)
        df['community'] = df['community'].str.lower()
        df.rename(columns={'pipe_construction_year':'construction_year',
                           'pipe_length':'length',
                           'contentstype':'content',
                           'width_mm':'width',
                           'community':'location',
                           'sewer_inspection_id':'inspection_id',
                           }, inplace=True)
                
        # Delete unnecessary columns:
        df = df.drop(columns=['kar1', 'kar2', 'kwan1','kwan2', 'klok1', 'klok2','height_mm','material_org','position','end_position','systemtype'])
        # change material names:
        df.loc[df['material'] == 'CONCRETE', 'material'] = 'Concrete'
        df.loc[df['material'] == 'PLASTIC', 'material'] = 'PVC'
        # Change content names:
        df.loc[df['content'] == 'mixed', 'content'] = 'MixedSewer'
        df.loc[df['content'] == 'wastewater', 'content'] = 'WasteSewer'
        df.loc[df['content'] == 'rainwater', 'content'] = 'RainSewer'
        df.loc[df['content'] == 'unknown', 'content'] = 'Unknown'
        
    df.rename(columns={'id': 'pipe_id','contentstype': 'content'}, inplace=True)
    
    if apply_filters:
        df = df[df['construction_year'].notna() & (df['length'] <= 100) & (df['width'] <= 1000) & (df['construction_year'] >= 1940)].reset_index(drop=True)
        df = df[df['width']>=50].reset_index(drop=True) #--> Minimal diameter allowed of 5 cm
        df = df[df['length']>=1].reset_index(drop=True) #--> Minimal length allowed of 1 meter
        df.loc[:, 'width'] = df['width'] / 1000  # Convert to meters where appropriate
        df.loc[:, 'age_today'] = datetime.datetime.now().year - df['construction_year']
        df.loc[:, 'material'] = df['material'].where(df['material'].isin(['Concrete', 'PVC']), 'Other')
        df.loc[:, 'content'] = df['content'].replace('Unknown', 'Other')

    if sub_network:
        coords = pd.DataFrame(extract_wkt_coords(df, 'wkt'), columns=['x_i', 'y_i', 'x_f', 'y_f'])
        bounds = (1.0616E5, 1.1879E5, 3.9330E5, 4.0519E5)
        mask = (coords[['x_i', 'x_f']].ge(bounds[0]) & coords[['x_i', 'x_f']].le(bounds[1])).all(axis=1) & \
               (coords[['y_i', 'y_f']].ge(bounds[2]) & coords[['y_i', 'y_f']].le(bounds[3])).all(axis=1)
        df = df[mask]
        coords = coords[mask]
        df = pd.concat([df, coords], axis=1)
        # Normalize network coordinates:
        if norm_coordinates:
            '''
            Normalize coordinates
            '''
            df = normalize_coordinates(df)
        df = df.drop(columns=['wkt'])
    
    if build_cohorts:
        # conditions = [
        #     (df['content'] == 'WasteSewer') & (df['material'] == 'Concrete'),
        #     (df['content'] == 'MixedSewer') & (df['material'] == 'Concrete'),
        #     (df['content'] == 'RainSewer') & (df['material'] == 'Concrete'),
        #     (df['content'] == 'WasteSewer') & (df['material'] == 'PVC'),
        #     (df['content'] == 'MixedSewer') & (df['material'] == 'PVC'),
        #     (df['content'] == 'RainSewer') & (df['material'] == 'PVC'),
        # ]
        # cohorts = ['ConWaste', 'ConMix', 'ConRain', 'PVCWaste', 'PVCMix', 'PVCRain']
        conditions = [
            ((df['content'] == 'WasteSewer') | (df['content'] == 'MixedSewer')) & (df['material'] == 'Concrete'),
            (df['content'] == 'RainSewer') & (df['material'] == 'Concrete'),
            ((df['content'] == 'WasteSewer') | (df['content'] == 'MixedSewer')) & (df['material'] == 'PVC'),
            (df['content'] == 'RainSewer') & (df['material'] == 'PVC'),
        ]            
        cohorts = ['CMW', 'CS', 'PMW', 'PS']
        df['cohort'] = np.select(conditions, cohorts, default=np.nan)
        # Replace 'nan' string with actual np.nan
        df['cohort'].replace('nan', np.nan, inplace=True)
        
    df = df[~df['cohort'].isna()]
    df = df.reset_index(drop=True) 
    
    if 'location' not in df.columns:
        df['location'] = 'breda' # --> assume as default location: breda
        
    if system_features_var_blacklist:
        for var, values in system_features_var_blacklist.items():
            df = df[~df[var].isin(values)]
        
    #df = df.sample(n=100) #--> Remove this later
    
    return df



def gen_comparative_table_policies(AllPolicies,path_name=''):
     results = {
         'Environment':[],
         'Prognostic model':[],
         'RL algorithm':[],
         'Mean Cum. Reward':[],
         'Std. Cum. Reward':[],
         }
     
     for k in list(AllPolicies.keys()):
         R = [AllPolicies[k][_]['Reward'].cumsum().iloc[-1] for _ in list(AllPolicies[k].keys())]
         results['Environment'].append(k[0][0])
         results['Prognostic model'].append(k[0][1])
         results['RL algorithm'].append(k[1])
         results['Mean Cum. Reward'].append(np.mean(R))
         results['Std. Cum. Reward'].append(np.std(R))
         
     results = pd.DataFrame(results)
     with open(path_name+generate_experiment_name_from_model()+'_results.tex', 'w') as f:
         f.write(results.to_latex(index=False,float_format="%.3f"))


def load_inspection_data(db_path=None,df_path=None,network=pd.DataFrame(),table='sewer_inspection_observations'):
    if len(network) == 0:
        network = load_network_data(db_path)
    if db_path:
        df = fetch_table_contents(db_path,table)
        # Rename columns:
        df.rename(columns={'sewer_inspection_id': 'inspection_id', 'sewer_pipe_id': 'pipe_id'}, inplace=True)
        # Drop unnecessary columns:
        df = df.drop(columns=['kar1', 'kar2', 'kwan1', 'kwan2', 'klok1', 'klok2', 'remark','id'])
        # Filter pipes:
        df = df[df['pipe_id'].isin(network['pipe_id'])]
        # Update self.network given the available amount of inspected pipes.
        network = network[network['pipe_id'].isin(df['pipe_id'])]
        network = network.reset_index(drop=True)
        # Reset index after filtering:
        df = df.reset_index(drop=True)
        # Correct pristine label using .loc for proper assignment:
        df.loc[df['damage_class'] == 0, 'damage_class'] = 1
        
        if 'location' not in df.columns:
            df['location'] = 'breda' # --> assume as default location: breda
        
    elif df_path:
        df = network
        df = df.drop(columns=['construction_year', 'cohort', 'length', 'width']) 
        df.loc[df['damage_class'] == 0, 'damage_class'] = 1
    return df



def discretize_dataframe(df, delta):
    """
    Discretizes the time index of the DataFrame based on the delta value(s).
    Delta can be a single value or a list/numpy array indicating the intervals.

    Parameters:
    - df: DataFrame with a time index, a 'state' column, and a 'count' column.
    - delta: Single value (int/float) for uniform discretization or list/numpy array for variable intervals.

    Returns:
    - DataFrame with discretized time index and adjusted counts per state.
    """

    # Function to map time values to discretized intervals
    def map_to_interval(time_val, intervals):
        # Find the interval that the time_val belongs to, return the start of this interval
        index = np.searchsorted(intervals, time_val, side='right') - 1
        return intervals[max(0, index)]

    # Check if delta is a list or numpy array, and handle accordingly
    if isinstance(delta, (list, np.ndarray)):
        # Calculate the unique sorted intervals including the max time to ensure coverage
        unique_intervals = np.unique(delta + [df.index.max() + (delta[-1] - delta[-2])])
        # Map each time value to its discretized interval
        df.index = df.index.map(lambda x: map_to_interval(x, unique_intervals))
    else:
        # Discretize time index by rounding to nearest multiple of delta
        df.index = (df.index // delta) * delta

    # Group by the new time index and state, then sum the counts
    df_discretized = df.groupby([df.index, 'state']).sum().reset_index().set_index('time')

    return df_discretized

def getFrequencyTable(y, states, delta=None):
    """
    Generates a frequency table for given data.

    Parameters:
    - y (DataFrame): Input data frame with 'count' and 'state' columns, indexed by 'time'.
    - states (list): A list of unique states to be included as columns in the output table.
    - delta (float, optional): The delta value for discretization. If provided, discretizes the 'time' index.

    Returns:
    - DataFrame: A pivot table with 'time' as rows, states as columns, normalized counts of each state per 'time', 
      and an additional column for the total count per 'time'. If delta is provided, the 'time' index is adjusted accordingly.
    """
    
    if delta:
        # Perform discretization based on a provided delta.
        y  = discretize_dataframe(y,delta)
    
    # Normalize counts within each 'time' group and pivot
    def normalize_counts(group):
        count_sum = group['count'].sum()
        group['normalized_count'] = group['count'] / count_sum
        return group

    # Apply normalization
    normalized = y.groupby(y.index, group_keys=False).apply(normalize_counts)

    # Pivot the table to get states as columns and times as rows, ensuring columns are in the order of 'states'
    pivot_table = normalized.pivot_table(index=normalized.index, columns='state', values='normalized_count', fill_value=0)

    # Ensure the pivot_table columns are ordered according to 'states'
    pivot_table = pivot_table.reindex(columns=states, fill_value=0)

    # Add the total counts per 'time' as an additional column
    pivot_table['total_count'] = y.groupby(y.index)['count'].sum()
    
    if delta:
        # Adjust the index
        pivot_table.index = pivot_table.index + delta/2
    
    return pivot_table

def print_analysis_results(results):
    """
    Nicely prints the analysis results of DataFrame columns obtained from the analyze_features function.

    Parameters:
    - results (dict): The analysis results where keys are column names and values are dictionaries with analysis details.
    """
    from prettytable import PrettyTable

    # Create a table with headers
    table = PrettyTable()
    table.field_names = ["Column Name", "Type", "Nature", "Additional Info"]

    for column, details in results.items():
        additional_info = "; ".join([f"{key}: {value}" for key, value in details.items() if key not in ['type', 'nature']])
        table.add_row([column, details.get('type', 'N/A'), details.get('nature', 'N/A'), additional_info])

    print(table)    
    
    
def transition_rate(i, t, param, function='gompertz'):
    """
    Calculates the transition rate for a given state, time, and function.
    
    Parameters:
        i (int): Index of the transition rate to calculate.
        t (float): Time at which the rate is evaluated.
        function (str, optional): The probability density function to use. Defaults to 'gompertz'.
    
    Returns:
        float: The calculated transition rate.
    
    Raises:
        NotImplementedError: If the function is not implemented.
    """
    if function == 'gompertz':
        from probability_density_functions import  gompertz
        return gompertz.hazard_rate(t, param['a'][i], param['b'][i])
    elif function == 'weibull':
        from probability_density_functions import  weibull
        return weibull.hazard_rate(t, param['a'][i], param['b'][i])
    elif function == 'gamma':
        from probability_density_functions import  gamma
        return gamma.hazard_rate(t, param['a'][i], param['b'][i])
    elif function == 'loglogistic':
        from probability_density_functions import  loglogistic
        return loglogistic.hazard_rate(t, param['a'][i], param['b'][i])
    elif function == 'lognormal':
        from probability_density_functions import  lognormal
        return lognormal.hazard_rate(t, param['a'][i], param['b'][i])
    elif function == 'exponential':
        from probability_density_functions import  exponential
        return exponential.hazard_rate(t, param['a'][i])
    else:
        raise NotImplementedError(f"Distribution '{function}' not implemented.")


def random_mass_function(N):
    """
    Generate an N-dimensional numpy array (vector) with elements that sum to 1.0.

    Parameters:
    N (int): The dimension of the vector to be generated.

    Returns:
    numpy.ndarray: An N-dimensional vector with elements that sum to exactly 1.0.
    """
    vector = np.random.rand(N)
    vector /= vector.sum()
    return vector

def compare_dataframes(df1, df2, atol=1e-5):
    """
    Compares two DataFrames to check if they are equal within a specified absolute tolerance.
    
    Parameters:
    - df1: First DataFrame to compare.
    - df2: Second DataFrame to compare.
    - atol: Absolute tolerance parameter for numerical comparison. Defaults to 1e-5.
    
    Returns:
    - True if DataFrames are equal within the specified absolute tolerance, else False.
    """
    # Ensure the same order of columns for comparison
    df1, df2 = df1.sort_index(axis=1), df2.sort_index(axis=1)

    # Compare categorical columns
    categorical_columns = df1.select_dtypes(exclude=[np.number]).columns
    if not df1[categorical_columns].equals(df2[categorical_columns]):
        return False

    # Prepare numerical columns for comparison
    df1_numerical = df1.apply(pd.to_numeric, errors='coerce')
    df2_numerical = df2.apply(pd.to_numeric, errors='coerce')

    # Compare numerical columns using np.allclose
    return np.allclose(df1_numerical, df2_numerical, atol=atol, equal_nan=True)


def generate_random_string(N):
    """
    Generate a random string of specified length.

    Parameters:
    N (int): The length of the string to be generated.

    Returns:
    str: A string consisting of randomly chosen letters and digits.
    """
    characters = string.ascii_letters + string.digits
    return ''.join(random.choice(characters) for _ in range(N))


def get_script_path():
    """
    Returns the absolute path of the currently executing script.
    
    :return: Absolute path of the script
    :rtype: str
    """
    return os.path.realpath(__file__)

def count_damage_points(arr: list, states=[]) -> pd.DataFrame:
    """
    Calculates the count and frequency of elements in a given list, optionally ensuring specific states are included.
    
    Parameters:
    - arr (list): The input list containing elements to count.
    - states (list, optional): A list of states to ensure are included in the output. Defaults to an empty list.
    
    Returns:
    - pd.DataFrame: A DataFrame with two columns ('count' and 'frequency') representing the count and frequency of each element in the input list. Ensures that all specified states are included, setting missing states' counts to 0 if they do not appear in the input list.
    """
    # Convert the input list to a pandas Series and count occurrences
    counts = pd.Series(arr).value_counts().sort_index(key=lambda x: x.astype(str))
    # Ensure all specified states are included, setting missing states' counts to 0
    counts = counts.reindex(counts.index.union(states), fill_value=0)
    # Calculate the frequency of each element
    total = counts.sum()
    frequencies = counts / total if total > 0 else 0
    # Create the DataFrame
    return  pd.DataFrame({'count': counts, 'frequency': frequencies})

def generate_vector_exact_step(start, stop, step):
    """
    Generates a vector with an exact step size, avoiding floating-point precision issues.
    
    Parameters:
    - start: The starting value of the vector.
    - stop: The end value of the vector (inclusive).
    - step: The step size between each element in the vector.
    
    Returns:
    - A list containing the generated vector with exact step sizes.
    """
    # Scale factors to avoid floating-point issues
    scale_factor = 1 / step
    
    # Calculate the number of steps including the stop value
    num_steps = int((stop - start) * scale_factor) + 1
    
    # Generate the vector using list comprehension
    vector = [(start + i * step) for i in range(num_steps)]
    
    return vector

def renormalize_mass_function(mass_function, error_threshold=1E-5):
    """
    Renormalize the given mass function, ensuring no negative values and that the sum of each row equals 1.
    
    Parameters:
    - mass_function: numpy.ndarray, the mass function to be renormalized.
    - error_threshold: float, the maximum allowed error for renormalization.
    
    Returns:
    - numpy.ndarray, the renormalized mass function.
    
    Raises:
    - ValueError: If the error after renormalization exceeds the error_threshold.
    """
    corrected_mass_function = np.where(mass_function < 0, 0, mass_function)
    sum_per_row = corrected_mass_function.sum(axis=1).reshape(-1, 1)
    error = np.abs(sum_per_row - 1)
    if np.any(error > error_threshold):
        raise ValueError(f"Renormalization error exceeds threshold: {error_threshold}")
    renormalized_mass_function = corrected_mass_function / sum_per_row
    return renormalized_mass_function

def generate_experiment_name_from_model(prefix: str = "exp"):
    """Generate a unique experiment name based on the model's hyperparameters, date, and time."""
    now = datetime.datetime.now()
    date_time_str = now.strftime("%Y%m%d_%H%M%S")
    experiment_name = f"{prefix}_{date_time_str}"
    return experiment_name


def compute_policy_single_run(args):
    """
    Computes the policy for a single run in a reinforcement learning setup.

    This function is designed to be executed in parallel, allowing for multiple
    simulations to be run concurrently. It takes a tuple of arguments to facilitate
    easy use with concurrent.futures.ProcessPoolExecutor.

    Parameters:
    - args: A tuple containing the following elements:
        - env: A copy of the environment object, which should contain observation_space_vars
               with 'group_id' attributes for 'state' and 'context', and support for 'reset',
               'step', and state normalization methods.
        - model: A copy of the model object used to predict the action given the current state.
                 It should support a 'predict' method.
        - run: The run number (integer).
        - verbose: Verbosity level (integer). If set to 2, the function prints the run number.
        - heuristics: A boolean flag indicating whether to use heuristics instead of the model
                      for action prediction.

    Returns:
    - A dictionary with a single key-value pair. The key is a string identifying the run,
      and the value is a Pandas DataFrame containing columns for time steps, context,
      current state, action taken, new state after the action, and the reward received.
    """
    env, model, run, verbose, heuristics = args
    state_space_index = env.observation_space_vars.loc[env.observation_space_vars['group_id'] == 'state', 'global_index'].values
    if env.EnvironmentParameters['add_context']:
        context_space_index = env.observation_space_vars.loc[env.observation_space_vars['group_id'] == 'context', 'global_index'].values

    if verbose == 2:
        print(f'Run No: {run}')
        
    env.reset()
    norm_state = env.state

    state = env.StateObj.setup.NormalizationManager.inverse_transform([norm_state[i] for i in state_space_index])
    if env.EnvironmentParameters['add_context']:
        context = env.ContextObj.NormalizationManager.inverse_transform([norm_state[i] for i in context_space_index])

    done = False
    t = 0
    df_time, df_context, df_state, df_action, df_new_state, df_reward, df_costs = [], [], [], [], [], [], []

    while not done:
        if not heuristics:
            action = model.predict(np.array(norm_state), deterministic=True)[0].item()
        else:
            action = model.predict(env)
            
        # if env.TempVars['damage_points']['current_state'].at['F', 'count'] > 0:
        #     action
        
        norm_new_state, reward, done, truncation, costs = env.step(action)
        
        new_state = env.StateObj.setup.NormalizationManager.inverse_transform([norm_new_state[i] for i in state_space_index])
        df_time.append(t)
        if env.EnvironmentParameters['add_context']:
            df_context.append(context)
        df_state.append(state)
        df_action.append(action)
        df_new_state.append(new_state)
        df_reward.append(reward)
        df_costs.append(costs)
        state = deepcopy(new_state)
        norm_state = deepcopy(norm_new_state)
        t += env.EnvironmentParameters['step_size']

    current_state_space = pd.concat(df_state, axis=0).reset_index(drop=True).rename(columns=lambda x: f'Current_State_{x}')
    new_state_space = pd.concat(df_new_state, axis=0).reset_index(drop=True).rename(columns=lambda x: f'New_State_{x}')
    flattened_data = [{'Maintenance_cost': item['costs']['maintenance_cost'], 'Inspection_cost': item['costs']['inspection_costs'], 'Replacement_cost': item['costs']['replacement_cost'], 'Failure_cost': item['costs']['failure_cost']} for item in df_costs]
    df_time_step = pd.DataFrame({'Time Step': df_time})
    df_action = pd.DataFrame({'Action': df_action})
    df_reward = pd.DataFrame({'Reward': df_reward})
    flattened_df = pd.DataFrame(flattened_data)
    dfs = [df_time_step, pd.concat(df_context, axis=0).reset_index(drop=True), current_state_space] if env.EnvironmentParameters['add_context'] else [df_time_step, current_state_space]
    dfs += [df_action, new_state_space, df_reward, flattened_df]
    result = pd.concat(dfs, axis=1)
    
    return result


def compute_policy_serialized(args):
    """
    Wrapper function to deserialize env and model and then compute the policy.
    Args are serialized versions of env, model, and any other arguments needed.
    """
    env_path, model_path, rl_alg, run_id, verbose, heuristics = args
    with open(env_path, 'rb') as f:
         env = dill.load(f)
    
    from stable_baselines3 import PPO, A2C, DQN
    alg_classes = {'PPO': PPO, 'A2C': A2C, 'DQN': DQN}
    model = alg_classes[rl_alg].load(model_path, custom_objects={"action_space": env.action_space, "observation_space": env.observation_space})
    
    return compute_policy_single_run((env, model, run_id, verbose, heuristics))

def compute_policy(env, model, rl_alg='PPO', runs=1, verbose=0, heuristics=False, max_workers=None):
    """
    Executes compute_policy in parallel by serializing env and model using dill.
    """
    
    results = []
    temp_dir = None
    
    try:
        if max_workers:
            from multiprocessing import Pool
            
            # Prepare temporary directory for serialized objects
            if not isinstance(env, str) or not isinstance(model, str):
                temp_dir = './temp_' + ''.join(random.choices(string.ascii_letters + string.digits, k=10))
                os.makedirs(temp_dir, exist_ok=True)
                env_path = os.path.join(temp_dir, 'env.dill')
                model_path = os.path.join(temp_dir, 'model.zip')
                
                with open(env_path, 'wb') as env_file:
                    dill.dump(env, env_file)
                model.save(model_path)
                
                env = env_path
                model = model_path

            args_list = [(env, model, rl_alg, run_id, verbose, heuristics) for run_id in range(runs)]
            
            # Execute the parallel computation
            with Pool(processes=max_workers) as pool:
                results = pool.map(compute_policy_serialized, args_list)
        
        else:
            # Fallback to single-threaded execution if no workers are specified
            for run_id in range(runs):
                results.append(compute_policy_single_run((env, model, run_id, verbose, heuristics)))
    
    except Exception as e:
        logging.error("An error occurred during policy computation: %s", e)
        raise
    finally:
        if temp_dir:
            # Clean up temporary files
            try:
                os.remove(env_path)
                os.remove(model_path)
                os.rmdir(temp_dir)
            except Exception as cleanup_error:
                logging.error("Error cleaning up temporary files: %s", cleanup_error)
    
    return results


# def compute_policy(env, model, rl_alg='PPO', runs=1, verbose=0, heuristics=False, max_workers=None):
#     """
#     Executes compute_policy in parallel by serializing env and model using dill.
#     """
    
#     if max_workers:
#         from multiprocessing import Pool
        
#         temp_dir = None
#         if not isinstance(env, str) or not isinstance(model, str):
#             temp_dir = './temp_'+''.join(random.choices(string.ascii_letters + string.digits, k=10))
#             if not os.path.exists(temp_dir):
#                 os.makedirs(temp_dir)
#             env_path = os.path.join(temp_dir, 'env.dill')
#             model_path = os.path.join(temp_dir, 'model.zip')
#             with open(env_path, 'wb') as env_file:
#                 dill.dump(env, env_file)
#             model.save(model_path)
#             env = env_path
#             model = model_path
            
#         # Prepare arguments for each parallel execution
#         args_list = [(env, model, rl_alg, run_id, verbose, heuristics) for run_id in range(runs)]
#         # Use multiprocessing Pool to execute in parallel
#         with Pool(processes=max_workers) as pool:
#             results = pool.map(compute_policy_serialized, args_list)
            
#         if temp_dir:
#             os.remove(env_path)
#             os.remove(model_path)
#             os.rmdir(temp_dir)
#     else:
#         results = []
#         for _ in range(runs):
#             results.append(compute_policy_single_run((env, model, _, verbose, heuristics)))
    
#     return results


def count_decimals(number):
    """
    Counts the number of decimal places in a given floating-point number.

    Args:
    - number (float): The floating-point number whose decimal places are to be counted.

    Returns:
    - int: The number of decimal places in the given number.
    """
    count = 0
    while number != int(number):
        number *= 10
        count += 1
    return count


def identify_step_size(vector):
    """
    Identifies the most common step size between consecutive elements in a given vector.

    Parameters:
    vector (array-like): The input vector from which to calculate step sizes.

    Returns:
    int/float/None: The most common step size found in the vector. Returns None if the vector has 1 or 0 elements.
    """
    if len(vector) > 1:
        step_sizes = np.diff(vector)
        return mode(step_sizes, keepdims=False)[0]
    else:
        return None

#%%
def fetchActionsRewards(policy: dict) -> pd.DataFrame:
    """
    Extracts and analyzes actions and rewards from a given policy, structuring the data into pandas DataFrames.

    This function processes a policy dictionary to identify all unique actions and time steps across the policy's
    components. It then calculates the occurrence of each action at every time step and aggregates rewards associated
    with these actions. The output includes two DataFrames: one for action occurrences over time and another for rewards
    received for each action over time.

    Parameters:
    - policy (dict): A policy dictionary containing 'Time Step', 'Action', and 'Reward' information for different keys.

    Returns:
    - Tuple[pd.DataFrame, pd.DataFrame]: A tuple of two pandas DataFrames. The first DataFrame contains counts of each
      action over the different time steps, and the second DataFrame contains the sum of rewards received for each
      action at each time step. The 'time_step' column serves as the index for both DataFrames.

    Note:
    - This function assumes that the input policy dictionary is structured with keys representing different
      scenarios or entities, each containing a dictionary with 'Time Step', 'Action', and 'Reward' as keys.
    - Actions are considered unique globally across all scenarios or entities in the policy.
    - The function internally converts lists or dictionaries within the 'Action' entries to strings for consistency.
    """
    # Concatenate time steps across all keys and find unique values
    time_step = pd.concat([pd.Series(policy[key]['Time Step']) for key in policy], axis=1).stack().unique()
    
    # Concatenate actions across all keys
    actions = pd.concat([pd.Series(policy[key]['Action']) for key in policy], axis=1)
    # Convert lists or dicts within actions to string, otherwise keep them unchanged
    actions = actions.applymap(lambda x: str(x) if isinstance(x, list) or isinstance(x, dict) else x)
    # Find unique actions
    unique_actions = pd.unique(actions.values.ravel('K'))

    # Prepare dictionary to hold counts of each action over time steps
    actions_over_time = {'action_' + str(action): [] for action in unique_actions}
    actions_over_time['time_step'] = list(time_step)

    # Count occurrences of each action for each time step
    for row in actions.index:
        row_data = actions.loc[row]
        for action in unique_actions:
            actions_over_time['action_' + str(action)].append((row_data == action).sum())

    # Convert the dictionary to DataFrame and set 'time_step' as index
    actions_over_time = pd.DataFrame(actions_over_time).set_index('time_step')
    
    # Rewards:
    rewards = pd.concat([pd.Series(policy[key]['Reward']) for key in policy], axis=1)
    rewards.columns = range(rewards.shape[1])
    actions.columns = range(actions.shape[1])
        
    reward_over_time = {'reward_action_' + str(action): [] for action in unique_actions}
    reward_over_time['time_step'] = list(time_step)
    
    for _ in rewards.index:
        for a in unique_actions:
            idx = actions.loc[_] == a
            reward_over_time['reward_action_' + str(a)].append(rewards.loc[_][idx].sum())
            
    reward_over_time  = pd.DataFrame(reward_over_time).set_index('time_step')

    # Validate if action counts across all time steps are consistent
    if len(actions_over_time.sum(axis=1).unique()) != 1:
        raise ValueError('The actions counts is inconsistent.')
    return actions_over_time, reward_over_time


#%% Compute error metrics:
def comp_error(y, yp, states, metric='RMSE', num_params=None):
    """
    Compute the error between ground truth and predicted values based on a specified metric.
    
    Parameters:
    - y (DataFrame): The ground truth data, containing at least 'state' and 'count' columns.
    - yp (DataFrame): The predicted probabilities for each state.
    - states (list): A list of the unique states to consider in the computation.
    - metric (str, optional): The metric to use for computing the error. Defaults to 'LogL' for log likelihood.
      Currently, only 'LogL' is supported.
    
    Returns:
    - float: The computed error based on the specified metric.
    
    Raises:
    - ValueError: If an unsupported error metric is specified.
    """
    metric = metric.lower()
    if metric == 'logl':
        return -log_likelihood(y, yp, states)
    elif metric == 'aic':
        return aic(y,yp,states,num_params)
    elif metric == 'bic':
        return bic(y,yp,states,num_params)
    elif metric == 'rmse':
        return root_mean_squared_error(y, yp, states)
    elif metric.lower() == 'mse':
        return mean_squared_error(y, yp, states)
    elif metric == 'se':
        return squared_error(y, yp, states)
    else:
        raise ValueError('The error_metric is not defined.')
        
def aic(y, yp, states, num_params):
    """
    Calculate the Akaike Information Criterion for a set of observations and predictions.

    Args:
    y (array-like): Observed values.
    yp (array-like): Predicted values, typically from a model.
    states (array-like): State information associated with each observation.
    num_params (int): Number of parameters in the model.

    Returns:
    float: The AIC score.
    """
    logl = log_likelihood(y, yp, states)
    return 2 * num_params - 2 * logl
    
def bic(y, yp, states, num_params):
    """
    Calculate the Bayesian Information Criterion for a set of observations and predictions.

    Args:
    y (array-like): Observed values.
    yp (array-like): Predicted values, typically from a model.
    states (array-like): State information associated with each observation.
    num_params (int): Number of parameters in the model.

    Returns:
    float: The BIC score.
    """
    logl = log_likelihood(y, yp, states)
    num_obs = y['count'].sum()  # Assuming 'count' is a column in y that sums to the total number of observations
    return np.log(num_obs) * num_params - 2 * logl


def differentiate_and_resample(x, y, num_points=100):
    """
    Differentiates the function y with respect to x by resampling to create
    a more evenly spaced x array, and then calculating the numerical derivative.
    
    Args:
    - x (np.array): The original x values.
    - y (np.array): The y values corresponding to x.
    - num_points (int): Number of points for resampling.

    Returns:
    - x_mid (np.array): The midpoints of the resampled x values.
    - dy_dx (np.array): The derivative of y with respect to the resampled x.
    """
    # Resample x and y to be more evenly spaced
    x_even = np.linspace(np.min(x), np.max(x), num_points)
    interpolate_y = interp1d(x, y, kind='cubic', fill_value="extrapolate")
    y_even = interpolate_y(x_even)
    
    # Calculate numerical derivative of the resampled data
    dx = np.diff(x_even)
    dy = np.diff(y_even)
    dy_dx = dy / dx
    x_mid = x_even[:-1] + dx / 2
    
    return x_mid, dy_dx

def log_likelihood(y, yp, states):
    """

    """
    yp = yp.cumsum(axis=1) # --> To obtain the survival curves
    ft = {s: differentiate_and_resample(x=np.array(yp.index), y=np.array(yp[s]), num_points=1000) for s in states}
    df = pd.DataFrame({s: -ft[s][1].T for s in states})
    df = df.set_index(ft[states[0]][0])
    t = np.array(yp.index)
    interp_values = {column: np.interp(t, df.index, df[column]) for column in df.columns}
    log = pd.DataFrame(interp_values, index=t)
    
    n = pd.DataFrame(0, columns=states, index=y.index)
    for s in states:
        n[s] = np.where(y['state'] == s, y['count'], 0)
        
    #n = n / n.sum()
        
    #y_grouped = y.groupby('state')['count'].sum()
    #W = y_grouped.max() / y_grouped
    #sum(W[s] * np.sum(np.log(yp.loc[y[y['state'] == s].index, s] + 1E-100) * y[y['state'] == s]['count']) for s in states)
    # sum(np.sum(np.log(yp.loc[y[y['state'] == s].index, s] + 1E-100) * y[y['state'] == s]['count']) for s in states)

    # Evaluate the data points:
    transitions = [(1,2),(1,'F'),(2,3),(2,'F'),(3,4),(4,'F'),(4,5),(5,'F')]
    log_likelihood = 0
    for s in states:
        for t in transitions:
            if s == t[0]:
                log_likelihood += (np.log(log[s]+1E-323) * n[t[1]]).sum()
    return log_likelihood


def squared_error(y, yp, states):
    """
    Calculate the squared error between actual and predicted values,
    adjusted by the 'total_count' from the predictions.
    
    Parameters:
    - y (pd.DataFrame): Actual values with states as indexes and categories as columns.
    - yp (pd.DataFrame): Predicted values with states as indexes, categories as columns, 
      and a 'total_count' column for weighting the error.
    - states (list): List of states to consider in the error calculation.
    
    Returns:
    - float: The weighted squared error.
    """
    if 'total_count' not in y.columns.tolist():
        y['total_count'] = 1
    
    return sum([((y[s]-yp[s])**2).multiply(y['total_count'],axis=0).sum() for s in states]) 

def mean_squared_error(y, yp, states):
    """
    Calculate the mean squared error between actual and predicted values,
    considering only specified states and adjusting for total counts in predictions.
    
    Parameters:
    - y (pd.DataFrame): Actual values with states as indexes and categories as columns.
    - yp (pd.DataFrame): Predicted values with states as indexes, categories as columns,
      and a 'total_count' column for normalization.
    - states (list): List of states to consider in the error calculation.
    
    Returns:
    - float: The calculated mean squared error.
    """
    return squared_error(y, yp, states) / (y['total_count'].sum() * len(states))

def root_mean_squared_error(y, yp, states):
    """
    Calculate the root mean squared error (RMSE) between actual and predicted values, 
    considering only specified states and adjusting for total counts in predictions.
    
    Parameters:
    - y (pd.DataFrame): Actual values with states as rows and categories as columns.
    - yp (pd.DataFrame): Predicted values with states as rows, categories as columns, 
      and a 'total_count' column for normalization.
    - states (list): List of states to consider in the error calculation.
    
    Returns:
    - float: The calculated RMSE value.
    """
    return np.sqrt(mean_squared_error(y, yp, states))

#%%
def capture_function_call(function, kwargs={}, output_path="."):
    """
    Captures a function call by generating a script that includes the function call with specified keyword arguments.

    Parameters:
    - function: The function to capture the call of.
    - kwargs (dict, optional): A dictionary of keyword arguments to pass to the function. Defaults to an empty dictionary.
    - output_path (str, optional): The output directory where the generated script should be saved. Defaults to the current directory.

    This function assumes that `generate_script_with_params` is correctly defined to handle these parameters.
    It creates the necessary directories if they do not exist and calls `generate_script_with_params` to generate the script.
    """
    os.makedirs(os.path.dirname(output_path), exist_ok=True)
    generate_script_with_params(function, kwargs, output_path)

def generate_script_with_params(function, params, output_path="."):
    """
    Generates a script from an existing Python script file, replacing the main function call with specified parameters.

    Parameters:
    - function: The function object whose call is to be captured in the generated script. The function's file location is used to read the original script.
    - params (dict): A dictionary of parameters to be passed to the function in the generated script.
    - output_path (str, optional): The directory where the generated script should be saved. Defaults to the current directory.
    """
    filename = function.__code__.co_filename
    with open(filename, 'r') as file:
        script_content = file.read()

    # Convert params dictionary to a string suitable for insertion into the script
    args_dict_str = ', '.join(f"{k}={repr(v)}" for k, v in params.items())
    new_args_call = f"args = vars(parse_args())\n    main({args_dict_str})"

    # Define the regex pattern to replace the main function call in the original script
    main_call_regex = re.compile(
        r"if __name__ == '__main__':.*?main\(.*?\)",
        re.DOTALL
    )

    # Replace the original main call with the new one
    modified_script = main_call_regex.sub(f"if __name__ == '__main__':\n    from multiprocessing import freeze_support\n    freeze_support()\n    {new_args_call}", script_content)

    # Ensure the output directory exists
    os.makedirs(output_path, exist_ok=True)
    new_filename = os.path.join(output_path, "TrainAgents.py")

    # Save the modified script
    with open(new_filename, 'w') as new_file:
        new_file.write(modified_script)
    
    print(f"Modified script saved as {new_filename}")
#%%
def eval_policy_kwargs(value, activation_function_name='ReLU'):
    """Evaluate and update policy kwargs with the specified activation function."""
    # Instantiate ActivationFunctions class
    activations = ActivationFunctions()
    # Dynamically get the activation function based on the name
    activation_fn = getattr(activations, activation_function_name.lower(), activations.relu)()
    # Evaluate the policy kwargs string to dict
    policy_kwargs = dict(activation_fn=activation_fn,
                         net_arch=eval(value))
    return policy_kwargs
#%% Activation functions
import torch.nn as nn

class ActivationFunctions:
    def relu(self):
        """ReLU (Rectified Linear Unit)
        f(x) = max(0, x)
        """
        return nn.ReLU

    def leaky_relu(self):
        """LeakyReLU
        f(x) = x if x > 0 else alpha * x
        """
        return nn.LeakyReLU

    def elu(self):
        """ELU (Exponential Linear Unit)
        f(x) = x if x >= 0 else alpha * (exp(x) - 1)
        """
        return nn.ELU

    def selu(self):
        """SELU (Scaled Exponential Linear Unit)
        Self-normalizing activation function
        """
        return nn.SELU

    def gelu(self):
        """GELU (Gaussian Error Linear Unit)
        f(x) = 0.5 * x * (1 + tanh(sqrt(2 / pi) * (x + 0.044715 * x^3)))
        """
        return nn.GELU

    def sigmoid(self):
        """Sigmoid
        f(x) = 1 / (1 + exp(-x))
        """
        return nn.Sigmoid

    def softmax(self):
        """Softmax
        Softmax is applied to the last dimension
        """
        return nn.Softmax #--> You need to add the dim argument.

    def softplus(self):
        """Softplus
        f(x) = log(1 + exp(x))
        """
        return nn.Softplus

    def prelu(self):
        """PReLU (Parametric ReLU)
        f(x) = x if x > 0 else alpha * x; alpha is a learnable parameter
        """
        return nn.PReLU

    def tanh(self):
        """Tanh
        f(x) = (exp(x) - exp(-x)) / (exp(x) + exp(-x))
        """
        return nn.Tanh
    
#%% Learning rate schedulers:
class Scheduler:
    def __init__(self):
        """
        Initializes a Scheduler object with a dictionary mapping schedule names to their corresponding methods.
        """
        self.schedules = {
            'linear': self.linear_schedule,
            'polynomial': self.polynomial_schedule,
            'exponential': self.exponential_schedule,
            'step': self.step_schedule,
            'constant': self.constant_schedule
        }

    def constant_schedule(self, initial_value: float) -> Callable[[float], float]:
        """
        Returns a constant schedule function that always returns the initial_value.

        Parameters:
        - initial_value (float): The value to be returned by the schedule function.

        Returns:
        - Callable[[float], float]: A function that takes progress_remaining as input and returns initial_value.
        """
        def func(_progress_remaining: float) -> float:
            return initial_value
        return func

    def linear_schedule(self, initial_value: float) -> Callable[[float], float]:
        """
        Returns a linear schedule function that linearly decreases the value based on the progress remaining.

        Parameters:
        - initial_value (float): The starting value at the beginning of the schedule.

        Returns:
        - Callable[[float], float]: A function that takes progress_remaining as input and returns a value that linearly interpolates to 0 based on initial_value.
        """
        def func(progress_remaining: float) -> float:
            return progress_remaining * initial_value
        return func

    def polynomial_schedule(self, initial_value: float, power=2) -> Callable[[float], float]:
        """
        Returns a polynomial schedule function that decreases the value based on the progress remaining raised to a given power.

        Parameters:
        - initial_value (float): The starting value at the beginning of the schedule.
        - power (int, optional): The power to which the progress_remaining is raised. Defaults to 2.

        Returns:
        - Callable[[float], float]: A function that takes progress_remaining as input and returns a value that decreases polynomially based on initial_value.
        """
        def func(progress_remaining: float) -> float:
            return (progress_remaining ** power) * initial_value
        return func

    def exponential_schedule(self, initial_value: float, decay_rate=0.5) -> Callable[[float], float]:
        """
        Returns an exponential schedule function that decreases the value based on an exponential decay function of the progress remaining.

        Parameters:
        - initial_value (float): The starting value at the beginning of the schedule.
        - decay_rate (float, optional): The rate of exponential decay. Defaults to 0.5.

        Returns:
        - Callable[[float], float]: A function that takes progress_remaining as input and returns a value that decreases exponentially based on initial_value.
        """
        def func(progress_remaining: float) -> float:
            return (decay_rate ** (1 - progress_remaining)) * initial_value
        return func

    def step_schedule(self, initial_value: float, drop_rate=0.5, step_size=0.25) -> Callable[[float], float]:
        """
        Returns a step schedule function that decreases the value in steps based on the progress remaining.

        Parameters:
        - initial_value (float): The starting value at the beginning of the schedule.
        - drop_rate (float, optional): The factor by which the value is reduced at each step. Defaults to 0.5.
        - step_size (float, optional): The size of each step as a proportion of the total progress. Defaults to 0.25.

        Returns:
        - Callable[[float], float]: A function that takes progress_remaining as input and returns a value that decreases in steps based on initial_value.
        """
        def func(progress_remaining: float) -> float:
            step = int((1 - progress_remaining) / step_size)
            return (drop_rate ** step) * initial_value
        return func

    def get(self, name: str, initial_value: float, **kwargs) -> Callable[[float], float]:
        """
        Returns a schedule function specified by name, initialized with the given initial value and any additional keyword arguments.

        Parameters:
        - name (str): The name of the schedule function to return.
        - initial_value (float): The initial value for the schedule function.
        - **kwargs: Additional keyword arguments specific to the schedule function.

        Returns:
        - Callable[[float], float]: The specified schedule function.

        Raises:
        - ValueError: If the specified schedule name does not exist.
        """
        if name not in self.schedules:
            raise ValueError(f"Invalid schedule name: {name}")
            
        self.name = name
        self.initial_value = initial_value
        
        return self.schedules[name](initial_value, **kwargs)
    
    def plot(self,**kwargs):
        """
        Plots the learning rate behavior of a schedule given its name, initial value, and any additional arguments.

        Parameters:
        - name (str): The name of the schedule.
        - initial_value (float): The initial value for the schedule, often representing the learning rate at the start.
        - **kwargs: Additional keyword arguments specific to the schedule function.
        """
        name = kwargs.get('name',self.name) 
        initial_value = kwargs.get('initial_value',self.initial_value) 
        if name not in self.schedules:
            raise ValueError(f"Invalid schedule name: {name}")
        schedule_func = self.get(name, initial_value, **kwargs)
        progress_remaining = [i * 0.01 for i in range(100)]
        values = [schedule_func(1 - i) for i in progress_remaining]
        plt.plot(progress_remaining, values)
        plt.xlabel('Progress Remaining')
        plt.ylabel('Learning Rate')
        plt.title(f'{name.capitalize()} Schedule')
        plt.show()
        
        
#%%
def load_wandb_callback(Obj):
    """
    Initializes and configures the Weights and Biases (WandB) callback for training monitoring.
    
    Args:
    Obj (object): An object that contains configuration settings and paths required for WandB setup.
    
    Returns:
    WandBCallback: A configured WandB callback object if logging is enabled in Obj configuration.
    None: Returns None if logging is disabled or WandB login fails.
    
    This function handles the login process to WandB with a specified API key, switches to offline mode if the login fails, 
    and configures a WandB callback object using settings from Obj if logging is enabled.
    """
    if Obj.config['log_wandb']:
        
        from WandBCallback import WandBCallback
        import wandb
        import warnings
        from copy import deepcopy
    
        success = wandb.login(key='82cc2d8c7bd3728badcb4372ebdf7ff2c4be9c9f', relogin=True)
        if not success:
            warnings.warn(f"Not possible to connect with Web and Biases, storing results locally in {Obj.save_path_dir}/wandb")
            wandb.offline()
            
        Obj.params = deepcopy(Obj.config)
        Obj.params = {k: v for k, v in Obj.params.items() if k not in ['current_script_path','tensorboard_log','experiment_setup','policy','policy_evaluation','policy_kwargs','progress_bar','save_path_dir','verbose']}
        Obj.project_id
        return WandBCallback(Obj, wandb)
    else:
        return None


def load_check_point(Obj):
    """
    Loads the checkpoint for the given object if it has any.

    Args:
    Obj (object): An object that must have a `check_point` attribute which indicates
                  whether to load a checkpoint. The object should also contain `save_path_dir`,
                  `config`, and `algorithm` attributes used to configure the checkpoint.

    Returns:
    SaveCheckPoint|None: Returns a configured SaveCheckPoint callback if `check_point` is True.
                         Returns None if `check_point` is False.
    """
    from SaveCheckPoint import SaveCheckPoint

    if Obj.check_point:
        save_path = os.path.join(Obj.save_path_dir, 'check_point')
        os.makedirs(save_path, exist_ok=True)
        checkpoint_callback = SaveCheckPoint(
            check_freq=Obj.config['n_steps'], 
            save_path=save_path,
            log_dir=Obj.config['tensorboard_log'],
            alg=Obj.algorithm, 
            verbose=Obj.config['verbose']
        )
        return checkpoint_callback
    else:
        return None
    
    
        
# def save_csv_file_hyperaparam_opt(Obj):
#     """

#     """
#     from SaveCheckPoint import SaveCheckPoint
#     save_path = os.path.join(Obj.save_path_dir, 'check_point')
#     os.makedirs(save_path, exist_ok=True)
#     checkpoint_callback = SaveCheckPoint(
#         check_freq=Obj.config['n_steps'], 
#         save_path=save_path,
#         log_dir=Obj.config['tensorboard_log'],
#         alg=Obj.algorithm, 
#         verbose=Obj.config['verbose']
#     )
#     return checkpoint_callback


def id_model_type(job):
    if not job.config.get('fully_observable') and job.config.get('fix_context') == {}:
        model_type = 'CPOMDP'
    elif job.config.get('fully_observable') and job.config.get('fix_context') == {}:
        model_type = 'CMDP'
    elif not job.config.get('fully_observable') and job.config.get('fix_context') != '{}':
        if job.config.get('fix_context') == {"pipe_material":"concrete","pipe_content":"stormwater","pipe_length":40,"pipe_diameter":0.2}:
            _var = 'CS'
        elif job.config.get('fix_context') == {"pipe_material":"concrete","pipe_content":"mixed","pipe_length":40,"pipe_diameter":0.2}:
            _var = 'CMW'
        model_type = 'POMDP_'+_var
    elif job.config.get('fully_observable') and job.config.get('fix_context') != '{}':
        if job.config.get('fix_context') == {"pipe_material":"concrete","pipe_content":"stormwater","pipe_length":40,"pipe_diameter":0.2}:
            _var = 'CS'
        elif job.config.get('fix_context') == {"pipe_material":"concrete","pipe_content":"mixed","pipe_length":40,"pipe_diameter":0.2}:
            _var = 'CMW'
        model_type = 'MDP_'+_var
    return model_type