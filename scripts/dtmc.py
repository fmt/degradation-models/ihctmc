import pandas as pd
from lifelines import KaplanMeierFitter as KM
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import dill
import os
from markov_chain_structures import MarkovChainStructure
from markov_chain_calibration import MarkovChainCalibration
from scipy.integrate import odeint
from aux_functions import renormalize_mass_function, comp_error, getFrequencyTable, identify_step_size
from aux_common_functions_ihtmc_dtmc import compute_error, renormalize_rows, sample, fit
from scipy.linalg import fractional_matrix_power as multMat

class HomogeneousDiscreteTimeMarkovChain:
    """
    Represents an inhomogeneous time Markov chain model for simulating and analyzing
    state transitions over time with time-varying transition rates.
    
    This class integrates functionalities for loading data, fitting the model using
    various statistical methods, predicting future states, and handling input and
    output operations for model variables.
    """
    
    def __init__(self, df=pd.DataFrame(), hazard_function = None, MCStructureID ='ihtmc_s2_typeA',  verbose=1):
        """
        Initializes the InhomogeneousTimeMarkovChain with optional data and verbosity level.
        
        Parameters:
        - df (pd.DataFrame, optional): A pandas DataFrame to initialize the model. Default is an empty DataFrame.
        - verbose (int, optional): Verbosity level where 1 indicates verbose output and 0 silent operation. Default is 1.
        """
        self.df = df
        self.df_freq = pd.DataFrame()
        self.verbose = verbose
        self.list_models = pd.DataFrame()
        self.MCObj = self.load_MCObj(MCStructureID=MCStructureID,MCType='dtmc')
        
    def error(self, y=pd.DataFrame(), yp=pd.DataFrame(), metric='RMSE'):
        """
        Computes the error between actual and predicted values using a specified metric.
    
        Parameters:
        - self: The object instance.
        - y (pd.DataFrame, optional): The actual values. Defaults to an empty DataFrame.
        - yp (pd.DataFrame, optional): The predicted values. Defaults to an empty DataFrame.
        - metric (str, optional): The metric to use for computing error. Defaults to 'RMSE'.
    
        Returns:
        - The error computed using the specified metric between the actual and predicted values.
        """
        return compute_error(self, y=y, yp=yp, metric=metric)
        
    def load_MCObj(self,MCStructureID='ihtmc_s6_typeA',function=None,MCType='dtmc'):
        """
        Loads a Markov Chain Structure object with specified states, name, and function.
        
        Parameters:
        - states (list of int, optional): List of states in the Markov Chain. Default is [1,2,3,4,5,6].
        - name (str, optional): Name of the Markov Chain. Default is 'Type A'.
        - function (str, optional): The function used for calculating transition probabilities. Default is 'gompertz'.
        
        Returns:
        - MarkovChainStructure: An initialized Markov Chain Structure object.
        """
        return MarkovChainStructure(MCStructureID=MCStructureID,function=function,MCType=MCType)

    def fit(self, **kwargs):
        """
        Fits a model to the data.
    
        This function assigns the result of a fitting operation to the attribute `MCObj`. The fitting operation is performed by calling the `fit` function with `self` and a dictionary of arguments `kwargs`.
    
        Parameters:
        **kwargs: Variable keyword arguments passed to the `fit` function.
    
        Returns:
        None
        """
        self.MCObj = fit(self, args=kwargs)

    def fetch_data(self,df):
        """

        """
        if df.empty and not self.df.empty:
            return self.df
        else:
            raise ValueError('There is not data to use to fit the model.')
    
    def renormalize_rows(self, dataframe, tolerance=0.01):
        """
        Renormalizes the rows of a dataframe to ensure that the sum of values in each row is 1.0,
        within a specified tolerance.
    
        Parameters:
        - self: The object instance.
        - dataframe: The DataFrame to be renormalized.
        - tolerance (float, optional): The tolerance within which the sum of row values must be to 1.0. Defaults to 0.01.
    
        Returns:
        - A DataFrame with rows renormalized to sum to 1.0, within the specified tolerance.
        """
        return renormalize_rows(dataframe=dataframe, tolerance=tolerance)
    
    def predict(self, params=dict(), t=np.linspace(0, 50), complete_NaN=True, ConverenceDetails=False):
        """
        Predicts the state probabilities over time for a discrete-time Markov chain (DTMC).
    
        This method calculates the probability distribution of states over a specified time
        range, using either matrix power method or interpolation, based on the step size
        of the time vector.
    
        Parameters:
        - params (dict, optional): A dictionary containing 's0' (initial state distribution)
          and 'x' (external inputs). If not provided, these values are taken from the
          Markov chain object associated with this instance.
        - t (array_like, optional): A time vector over which the state probabilities are
          to be calculated. Defaults to a linearly spaced vector from 0 to 50.
        - complete_NaN (bool, optional): If True, any NaN values in the resulting probability
          distributions will be forward-filled. Defaults to True.
    
        Returns:
        - p (DataFrame): A pandas DataFrame containing the state probabilities over time,
          with each row corresponding to a time point specified in 't' and each column
          representing a state.
    
        Note:
        - The method used for prediction ('matrix_power' or 'interpolation_method') is
          determined based on whether the step size in 't' is an integer.
        """
        # Determine initial state and external inputs
        s0, x = (self.MCObj.s0, self.MCObj.x) if not params else (params['s0'], params['x'])
        
        # Adjust initial state if the first time point is not zero
        step_size = identify_step_size(t)
        steps = t
        method = 'interpolation_method' if step_size % 1 == 0 else 'matrix_power'
        s0 = pd.DataFrame(
            self.solve_dtmc(s0=s0, P=self.MCObj.P(), steps=[0, max(steps[0])], step_size=step_size, method=method),
            columns=self.MCObj.states
        ).iloc[-1].to_numpy() if t[0] != 0.0 else s0
        
        # Solve DTMC and organize results in a DataFrame
        p = pd.DataFrame(
            self.solve_dtmc(s0=s0, P=self.MCObj.P(), steps=steps, method=method),
            columns=self.MCObj.states
        )
        p = self.renormalize_rows(p)
        p.index = steps
        
        # Forward-fill NaN values if requested
        if complete_NaN and p.isnull().any().any():
            p.fillna(method='ffill', inplace=True)
        
        if ConverenceDetails:
            return p, True
        else:
            return p
    
    def solve_dtmc(self, s0, P, steps, method='matrix_power'):
        """
        Solves the discrete-time Markov chain (DTMC) for given steps and method.
    
        Parameters:
        - s0 (array-like): The initial state distribution.
        - P (matrix): Transition matrix of the DTMC.
        - steps (list): The time steps at which the DTMC is solved.
        - method (str): The method used to solve the DTMC. Can be 'matrix_power' or 'interpolation_method'. Defaults to 'matrix_power'.
    
        Returns:
        - s (ndarray): An array containing the state distributions at each step.
        """
        if not isinstance(s0, np.ndarray):
            s0 = np.array(s0)
        if method == 'interpolation_method':
            s = np.array([s0.dot(multMat(P, step)).T for step in list(steps)])
        elif method == 'matrix_power':
            s = np.array([s0.dot(np.linalg.matrix_power(P, step)).T for step in list(steps.astype(int))])
        return s


    def getTransitionProbabilityMatrix(self, params=dict(), t=np.linspace(0, 50), atol=1e-5, rtol=1e-5,output_format='dataframe'):
        """
        Computes and returns a pandas DataFrame of transition probabilities for a Markov chain. The DataFrame's index are the time steps,
        and the columns are named as tuples representing (from, to) state transitions. Each cell contains the transition probability
        for its corresponding (from, to) state pair at the given time step.
    
        Parameters:
        - params (dict): Dictionary with 's0' and 'x' to override self.MCObj's initial state and control parameter if provided.
        - t (np.ndarray): Time points array for which the transition probabilities are calculated.
        - atol (float): Absolute tolerance for the ODE solver.
        - rtol (float): Relative tolerance for the ODE solver.
    
        Returns:
        - DataFrame: Transition probabilities with times as index and (from, to) tuples as columns.
        """
        s0, x = (self.MCObj.s0, self.MCObj.x) if not params else (params['s0'], params['x'])
        state_len = len(self.MCObj.states)
        idMatrix = np.identity(state_len).flatten()
        P = {}
        for t_i, t_j in zip(t[0:-1],t[1:]):
            sol = odeint(self.dPdt, idMatrix, t=[t_i, t_j], args=(x, True), atol=atol, rtol=rtol)
            P[t_j] = renormalize_mass_function(sol[-1].reshape(-1,state_len))
        
        if output_format == 'dataframe':
            columns = [(from_state, to_state) for from_state in self.MCObj.states for to_state in self.MCObj.states]
            data = {i: P[i].reshape(state_len, state_len).flatten() for i in list(P.keys())}
            df = pd.DataFrame(data, index=columns).T
            return df
        elif output_format == 'dictionary':
            return P
    
    def sample(self, t=np.linspace(0, 50), n=1000, states=[], exact_t_spacing=False):
        """
        Samples observations based on the defined parameters and configurations of the model.
    
        Parameters:
        - self: The object instance.
        - t (array-like, optional): The time points for which to sample. Defaults to a linear space between 0 and 50.
        - n (int, optional): The number of observations to sample. Defaults to 1000.
        - states (list, optional): The specific states from which to sample. If empty, samples from all states. Defaults to an empty list.
        - exact_t_spacing (bool, optional): Determines if the sampling should occur at exact time points specified in `t` or at random times within the range defined by `t`. Defaults to False.
    
        Returns:
        - A DataFrame containing the sampled observations, structured to include the sampled times, states, and their frequencies.
        """
        return sample(self, t=t, n=n, states=states, exact_t_spacing=exact_t_spacing)


    def plot(self, t=np.arange(0, 50),atol=1e-4,rtol=1e-4):
        
        p = self.predict(t=t,atol=atol,rtol=rtol)
        plt.plot(p.index,p,label=self.MCObj.states)
        plt.xlabel('Time')
        plt.ylabel('State probability')
        plt.grid()
    
    def save(self, var=None, filename='variable', path=''):
        """
        Saves 'var' to a file using dill in the specified path.

        Args:
            var: The variable (typically a DataFrame) to save.
            filename (str, optional): The name of the file to save the variable to. Defaults to 'dataframe.dill'.
            path (str, optional): The directory path where the file will be saved. Defaults to the current directory.
        """
        if not var:
            var = self # --> save the whole object
            
        filename = filename + '.dill'
        full_path = os.path.join(path, filename)
        if not os.path.exists(path):
            os.makedirs(path)
        with open(full_path, 'wb') as file:
            dill.dump(var, file)
        print(f"Variable saved to {full_path}.")

    def load(self, filename='variable', path=''):
        """
        Loads a DataFrame from a file using dill from the specified path.

        Args:
            filename (str, optional): The name of the file to load the DataFrame from. Defaults to 'dataframe.dill'.
            path (str, optional): The directory path from where the file will be loaded. Defaults to the current directory.
        """
        filename = filename + '.dill'
        full_path = os.path.join(path, filename)
        try:
            with open(full_path, 'rb') as file:
                return dill.load(file)
            print(f"DataFrame loaded from {full_path}.")
        except FileNotFoundError:
            print(f"No file found with the name {full_path}.")
            

    
if __name__ == "__main__":
    model = HomogeneousDiscreteTimeMarkovChain()