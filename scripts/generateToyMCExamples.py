#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 15:22:52 2024

@author: lisandro
"""
from ihtmc import InhomogeneousTimeMarkovChain as IHTMC
from markov_chain_calibration import MarkovChainCalibration as MCCal
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import os
from mpl_toolkits.mplot3d import Axes3D
import random

plt.close('all')
#%% Functions
def random_color():
    return "#{:06x}".format(random.randint(0, 0xFFFFFF))
def generate_random_samples(n, bounds):
    return np.random.uniform(low=[b[0] for b in bounds], high=[b[1] for b in bounds], size=(n, len(bounds)))
#%% Input parameters:
t_from = 0
t_to   = 50
functions = ['lognormal','loglogistic','gompertz','exponential','weibull']
path='../../degradation_models/toy_models_ihtmc_s2_typeA'
MCStructureID = 'ihtmc_s2_typeA'
#%% Ground-truth Markov chain
ground_truth_MC = IHTMC(MCStructureID=MCStructureID)
ground_truth_MC.MCObj.x  = np.array([0.05,0.1])
ground_truth_MC.MCObj.s0 = np.array([0.9,0.1])
g = ground_truth_MC.predict(t=np.linspace(t_from,t_to,1000),atol=1e-4, rtol=1e-4)  # Removed colon at the end
#%% Randomly sample from Markov chain --> Generate synthetic dataset.
num_inspections = 10000
ti = np.array(g.index)
s = np.array([int(i[2:]) for i in g.columns])
times = np.random.uniform(t_from, t_to, num_inspections)
choices = [np.random.choice(s, p=g.iloc[np.argmin(np.abs(t - ti))].values) for t in times]
obs = np.column_stack((times, choices))
system_inspections = pd.DataFrame(obs, columns=['time', 'state']).sort_values(by='time', ascending=True).reset_index(drop=True)
system_inspections['state'] = system_inspections['state'].astype(int)
system_inspections.set_index('time', inplace=True)
#%% Markov chains models:
models = {}
for pdf in functions:
    models[pdf] = IHTMC(df=system_inspections,MCStructureID=MCStructureID,hazard_function=pdf)
    if os.path.exists(path+'/'+pdf+'.dill'):
        models[pdf] = models[pdf].load(filename=pdf,path=path)
    else:
        models[pdf].fit()
        models[pdf].save(filename=pdf,path=path)
#%% Plot multi-state degradation models:
t=np.linspace(t_from,t_to,1000)
plt.figure()
for pdf in functions:
    plt.plot(t,models[pdf].predict(t=t),color=random_color())
plt.plot(t,ground_truth_MC.predict(t=t),color='gray',linestyle='--')
