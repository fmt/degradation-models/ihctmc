#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 10:03:49 2024

@author: lisandro
"""
import pandas as pd
from lifelines import KaplanMeierFitter as KM
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.pyplot as plt
import dill
import os
from markov_chain_structures import MarkovChainStructure
from copy import deepcopy
from aux_functions import renormalize_mass_function, comp_error
from aux_common_functions_ihtmc_dtmc import compute_error, sample, renormalize_rows, fit
import time


def solve_diff_equations_with_solve_ivp(dPdt, t, s0, x, atol, rtol, reshape=False):
    """
    Solves differential equations using the `solve_ivp` function from the SciPy library.

    Parameters:
    - dPdt (function): The derivative of y with respect to t, i.e., dy/dt.
    - t (array): An array of time points at which to solve for y.
    - s0 (array): Initial state.
    - x (float): Parameter to be passed to the differential equation.
    - atol (float): Absolute tolerance for the solver.
    - rtol (float): Relative tolerance for the solver.
    - reshape (bool, optional): If True, additional dimensions are added to the output shape. Default is False.

    Returns:
    - (array, bool): A tuple containing the solution as an array and a boolean indicating success.
    """
    from scipy.integrate import solve_ivp

    t = deepcopy(t)
    t[0] = 1E-6 if t[0] == 0.0 else t[0]
    sol = solve_ivp(fun=dPdt, y0=s0, t_span=(min(t), max(t)), method='LSODA', args=(x, reshape), atol=atol, rtol=rtol, t_eval=t)
    if sol.success and not np.all(np.isnan(sol.y)):
        return np.array(sol.y.T), True
    else:
        return np.array([]), False

def solve_diff_equations_with_odeint(dPdt, t, s0, x, atol, rtol, reshape=False):
    """
    Solves differential equations using the `odeint` function from the SciPy library.

    Parameters:
    - dPdt (function): The derivative of y with respect to t, i.e., dy/dt.
    - t (array): An array of time points at which to solve for y.
    - s0 (array): Initial state.
    - x (float): Parameter to be passed to the differential equation.
    - atol (float): Absolute tolerance for the solver.
    - rtol (float): Relative tolerance for the solver.
    - reshape (bool, optional): If True, additional dimensions are added to the output shape. Default is False.

    Returns:
    - (array, bool): A tuple containing the solution as an array and a boolean indicating success.
    """
    from scipy.integrate import odeint
    sol, info = odeint(dPdt, s0, t, args=(x,reshape), atol=atol, rtol=rtol, full_output=True)
    success = info['message'] == 'Integration successful.'
    return sol, success


class InhomogeneousTimeMarkovChain:
    """
    Represents an inhomogeneous time Markov chain model for simulating and analyzing
    state transitions over time with time-varying transition rates.
    
    This class integrates functionalities for loading data, fitting the model using
    various statistical methods, predicting future states, and handling input and
    output operations for model variables.
    """
    
    def __init__(self, df=pd.DataFrame(), hazard_function ='gompertz', MCStructureID ='ihtmc_s2_typeA', verbose=1, DiffSolver='solve_ivp'):
        """
        Initializes the InhomogeneousTimeMarkovChain with optional data and verbosity level.
        
        Parameters:
        - df (pd.DataFrame, optional): A pandas DataFrame to initialize the model. Default is an empty DataFrame.
        - verbose (int, optional): Verbosity level where 1 indicates verbose output and 0 silent operation. Default is 1.
        - DiffSolver: it can be solve_ivp or odeint
        """
        self.df = df
        self.simple_time_vector = False 
        self.delta_t = 1
        self.df_freq = pd.DataFrame()
        self.verbose = verbose
        self.DiffSolver = DiffSolver
        self.list_models = pd.DataFrame()
        self.MCObj = self.load_MCObj(MCStructureID=MCStructureID,function=hazard_function,MCType='ihtmc')
        self.load_DiffEqsSolver()
        
        
    def load_MCObj(self,MCStructureID='ihtmc_s6_typeA',function='gompertz',MCType='ihtmc'):
        """
        Loads a Markov Chain Structure object with specified states, name, and function.
        
        Parameters:
        - states (list of int, optional): List of states in the Markov Chain. Default is [1,2,3,4,5,6].
        - name (str, optional): Name of the Markov Chain. Default is 'Type A'.
        - function (str, optional): The function used for calculating transition probabilities. Default is 'gompertz'.
        
        Returns:
        - MarkovChainStructure: An initialized Markov Chain Structure object.
        """
        return MarkovChainStructure(MCStructureID=MCStructureID,function=function,MCType=MCType)
        
    def solve_diff_equations_solve_ivp(self, t, s0, x, atol, rtol, reshape=False):
        """
        Solves differential equations using the solve_ivp method.
    
        Parameters:
        - t: Time points at which to solve the differential equations.
        - s0: Initial state.
        - x: Additional parameters passed to the differential equation.
        - atol: Absolute tolerance for the solver.
        - rtol: Relative tolerance for the solver.
        - reshape: Whether to reshape the output (default: False).
    
        Returns:
        - The solution of the differential equation using solve_ivp.
        """
        return solve_diff_equations_with_solve_ivp(self.dPdt, t, s0, x, atol, rtol, reshape)
    
    def solve_diff_equations_odeint(self, t, s0, x, atol, rtol, reshape=False):
        """
        Solves differential equations using the odeint method.
    
        Parameters:
        - t: Time points at which to solve the differential equations.
        - s0: Initial state.
        - x: Additional parameters passed to the differential equation.
        - atol: Absolute tolerance for the solver.
        - rtol: Relative tolerance for the solver.
        - reshape: Whether to reshape the output (default: False).
    
        Returns:
        - The solution of the differential equation using odeint.
        """
        return solve_diff_equations_with_odeint(self.dPdt, t, s0, x, atol, rtol, reshape)
        
    def load_DiffEqsSolver(self):
        """
        Loads the appropriate differential equations solver based on the DiffSolver attribute.
        
        Raises:
        - ValueError: If an unknown differential equations solver is specified.
        """
        if self.DiffSolver == 'solve_ivp':
            self.solve_diff_equations = self.solve_diff_equations_solve_ivp
        elif self.DiffSolver == 'odeint':
            self.solve_diff_equations = self.solve_diff_equations_odeint
        else:
            raise ValueError('Unknown differential equations solver.')

    def fit(self, **kwargs):
        """
        Fits a model to the data.
    
        This function assigns the result of a fitting operation to the attribute `MCObj`. The fitting operation is performed by calling the `fit` function with `self` and a dictionary of arguments `kwargs`.
    
        Parameters:
        **kwargs: Variable keyword arguments passed to the `fit` function.
    
        Returns:
        None
        """
        self.MCObj = fit(self, args=kwargs)

    def fetch_data(self,df,function):
        """
        Fetches data for analysis, validating its presence and suitability based on the specified function.
        
        This method ensures that data is available for modeling by either using the provided dataframe `df` or
        falling back to the instance's dataframe `self.df` if `df` is empty. It raises a ValueError if no data
        is available or if the specified modeling function is not supported.
        
        Args:
            df (DataFrame): The dataframe to be used for modeling. If empty, the instance's dataframe `self.df` is used.
            function (str): The type of modeling function to be applied. Currently supports 'exponential' for
                            Homogeneous-Time Markov Chains and other values for Inhomogeneous-Time Markov Chains.
        
        Raises:
            ValueError: If no data is provided and `self.df` is also empty, or if an unsupported function is specified.
        
        Returns:
            DataFrame: The dataframe to be used for further analysis and modeling.
        """
        if df.empty and not self.df.empty:
            return self.df
        else:
            if function == 'exponential':
                raise ValueError("You most provide data over which carry out the modelling via Homogeneous-Time Markov Chains")
            else:
                raise ValueError("You most provide data over which carry out the modelling via Inhomogeneous-Time Markov Chains")
    
    def compute_error(self, y=pd.DataFrame(), yp=pd.DataFrame(), metric='RMSE'):
        """
        Computes the error between actual and predicted values using a specified metric.
    
        Parameters:
        - self: The object instance.
        - y (pd.DataFrame, optional): The actual values. Defaults to an empty DataFrame.
        - yp (pd.DataFrame, optional): The predicted values. Defaults to an empty DataFrame.
        - metric (str, optional): The metric to use for computing error. Defaults to 'RMSE'.
    
        Returns:
        - The error computed using the specified metric between the actual and predicted values.
        """
        return compute_error(self, y=y, yp=yp, metric=metric)

    def renormalize_rows(self, dataframe, tolerance=0.01):
        """
        Renormalizes the rows of a dataframe to ensure that the sum of values in each row is 1.0,
        within a specified tolerance.
    
        Parameters:
        - self: The object instance.
        - dataframe: The DataFrame to be renormalized.
        - tolerance (float, optional): The tolerance within which the sum of row values must be to 1.0. Defaults to 0.01.
    
        Returns:
        - A DataFrame with rows renormalized to sum to 1.0, within the specified tolerance.
        """
        return renormalize_rows(dataframe=dataframe, tolerance=tolerance)
    
    def prepare_time_vector(self, t):
        """
        Convert the input time data into a numpy array of float32 type and adjust its range and interval if needed.
        
        Args:
        t (list or np.ndarray): Time data to be processed.
        
        Returns:
        np.ndarray: Processed numpy array of time data.
        """
        if isinstance(t, list):
            t = np.array(t)
            t = t.astype(np.float32)
        elif not isinstance(t, np.ndarray):
            t = np.array(t)
        if self.simple_time_vector:
            step = round((max(t) - min(t)) / self.delta_t)
            t = np.linspace(min(t), max(t), step)
        return t
    
    def interpolate_dataframe(self, df, t):
        """
        Interpolate a pandas DataFrame along a new time vector if simple_time_vector is True,
        otherwise return the DataFrame as is.
        
        Args:
        df (pd.DataFrame): DataFrame containing the data to interpolate.
        t (np.ndarray): New time vector for interpolation.
        
        Returns:
        pd.DataFrame: Interpolated DataFrame or original DataFrame based on simple_time_vector.
        """
        if self.simple_time_vector:
            interp_values = {column: np.interp(t, df.index, df[column]) for column in df.columns}
            return pd.DataFrame(interp_values, index=t)
        else:
            return df

    def predict(self, params=dict(), t=np.linspace(0, 50), atol=1e-5, rtol=1e-5, complete_NaN=True, ConverenceDetails=False, TryAllSolvers = True):
        """
        Predicts system dynamics over time using differential equations within a Markov Chain model.

        Parameters:
        - params (dict, optional): Parameters for the prediction. Should include 's0' for initial conditions and 'x' for other parameters.
          Defaults to an empty dict, in which case the method uses MCObj attributes.
        - t (array_like, optional): Time points at which to solve the differential equations. Defaults to numpy.linspace(0, 50).
        - atol (float, optional): Absolute tolerance for the ODE solver. Defaults to 1e-5.
        - rtol (float, optional): Relative tolerance for the ODE solver. Defaults to 1e-5.
        - complete_NaN (bool, optional): If True, fills NaN values in the output DataFrame with the last valid value. Defaults to True.
        - ConverenceDetails (bool, optional): If True, the function returns a tuple containing the prediction DataFrame and a success flag.
          Otherwise, it returns only the prediction DataFrame. Defaults to False.

        Returns:
        - pandas.DataFrame or tuple: The prediction as a pandas DataFrame with columns 's_i' for each state, indexed by time points.
          If ConverenceDetails is True, also returns a boolean indicating the success of the prediction.
          If complete_NaN is True and there are NaN values, fills NaN values with the last valid observation.
        """
        t_ = self.prepare_time_vector(t)
        s0, x = (self.MCObj.s0, self.MCObj.x) if not params else (params['s0'], params['x'])
        # Correct initial state if t[0] != 0
        s0 = pd.DataFrame(self.solve_diff_equations(t=[0,t_[0]],s0=s0,x=x,atol=atol,rtol=rtol)[0], columns=self.MCObj.states).iloc[-1].to_numpy() if t_[0] != 0.0 else s0   
        # Solve system of differential equations:
        sol, success = self.solve_diff_equations(t_,s0,x,atol,rtol)
        if TryAllSolvers and not success:
            self.DiffSolver = 'solve_ivp' if self.DiffSolver == 'odeint' else 'odeint'
            self.load_DiffEqsSolver()
            sol, success = self.solve_diff_equations(t_, s0, x, atol, rtol)
        if success:
            p = pd.DataFrame(sol, columns=self.MCObj.states)
            p = self.renormalize_rows(p)
            p.index = t_[0:p.shape[0]]
            p = self.interpolate_dataframe(p,t)    
            if complete_NaN and p.isnull().any().any():
                p.fillna(method='ffill', inplace=True)   
            if ConverenceDetails:
                return p, success
            else:
                return p
        else:
            if ConverenceDetails:
                return pd.DataFrame(), success
            else:
                return pd.DataFrame()
        
    def dPdt(self, a, b, x, reshape=False):
        """
        Computes the derivative of the probability matrix P with respect to time (t), 
        for use in an ODE solver within the context of a Markov Chain model.
    
        Parameters:
        - a (numpy.ndarray or float): Depending on the differential equation solver, 
          this could be the initial state probability vector/matrix or the current time point.
        - b (numpy.ndarray or float): Depending on the differential equation solver, 
          this could be the initial state probability vector/matrix or the current time point.
        - x: Additional parameters required by the Markov Chain's transition rate matrix method .Q.
        - reshape (bool, optional): If True, reshapes the initial state probability vector/matrix 
          from a vector to a square matrix corresponding to the number of states in the Markov Chain. Defaults to False.
    
        Returns:
        numpy.ndarray: The derivative of the probability matrix P at time `t`, as a flattened array.
        
        Note:
        - `self.MCObj` must be an instance of a Markov Chain class with a `.Q` method that computes
          the transition rate matrix Q given time `t` and additional parameters `x`.
        """
        if self.DiffSolver == 'solve_ivp':
            t, s0 = a, b
        elif self.DiffSolver == 'odeint':
            t, s0 = b, a
        else: 
            raise ValueError('Unknown differential equations solver')
        s0 = s0.reshape(-1, len(self.MCObj.states)) if reshape else s0
        Q = self.MCObj.Q(t, x)
        if np.isnan(Q).any() or np.isinf(Q).any():
            raise ValueError("Q contains NaN or infinite numbers.")
        return (s0 @ Q).flatten()

    def getTransitionProbabilityMatrix(self, params=dict(), t=np.linspace(0, 50), atol=1e-5, rtol=1e-5,output_format='dataframe'):
        """
        Computes and returns a pandas DataFrame of transition probabilities for a Markov chain. The DataFrame's index are the time steps,
        and the columns are named as tuples representing (from, to) state transitions. Each cell contains the transition probability
        for its corresponding (from, to) state pair at the given time step.
    
        Parameters:
        - params (dict): Dictionary with 's0' and 'x' to override self.MCObj's initial state and control parameter if provided.
        - t (np.ndarray): Time points array for which the transition probabilities are calculated.
        - atol (float): Absolute tolerance for the ODE solver.
        - rtol (float): Relative tolerance for the ODE solver.
    
        Returns:
        - DataFrame: Transition probabilities with times as index and (from, to) tuples as columns.
        """
        s0, x = (self.MCObj.s0, self.MCObj.x) if not params else (params['s0'], params['x'])
        state_len = len(self.MCObj.states)
        idMatrix = np.identity(state_len).flatten()
        P = {}
        for t_i, t_j in zip(t[0:-1],t[1:]):
            sol = self.solve_diff_equations(t=[t_i,t_j],s0=idMatrix,x=x,atol=atol,rtol=rtol,reshape=True)[0]
            P[t_j] = renormalize_mass_function(sol[-1].reshape(-1,state_len))
        
        if output_format == 'dataframe':
            columns = [(from_state, to_state) for from_state in self.MCObj.states for to_state in self.MCObj.states]
            data = {i: P[i].reshape(state_len, state_len).flatten() for i in list(P.keys())}
            df = pd.DataFrame(data, index=columns).T
            return df
        elif output_format == 'dictionary':
            return P
            
    def getTransitionRateMatrix(self, params=dict(), t=np.linspace(0, 50, 51), atol=1e-5, rtol=1e-5, output_format='dataframe'):
        """
        Computes the transition rate matrix for a Markov Chain (MC) over a specified time interval.
    
        Parameters:
        - params (dict, optional): A dictionary containing the initial state 's0' and any other parameters 'x' required for the calculation.
                                   If empty, the default values from MCObj will be used.
        - t (numpy.ndarray, optional): A numpy array specifying the time points at which the transition rates are calculated. Default is a linearly spaced array from 0 to 50 with 51 points.
        - atol (float, optional): Absolute tolerance for the calculation. Default is 1e-5.
        - rtol (float, optional): Relative tolerance for the calculation. Default is 1e-5.
        - output_format (str, optional): Format of the output. Default is 'dataframe'.
    
        Returns:
        - A transition rate matrix in the specified output format. The computation uses parameters from either the 'params' dictionary or the MCObj's default values.
        """
        s0, x = (self.MCObj.s0, self.MCObj.x) if not params else (params['s0'], params['x'])
        return self.MCObj.Q(t, x, output_format='dataframe')
    
    def getHazardRates(self, params=dict(), t=np.linspace(0, 50, 51), atol=1e-5, rtol=1e-5, output_format='dataframe'):
        """
        To be done.
        """
        pass

    def sample(self, t=np.linspace(0, 50), n=1000, states=[], exact_t_spacing=False):
        """
        Samples observations based on the defined parameters and configurations of the model.
    
        Parameters:
        - self: The object instance.
        - t (array-like, optional): The time points for which to sample. Defaults to a linear space between 0 and 50.
        - n (int, optional): The number of observations to sample. Defaults to 1000.
        - states (list, optional): The specific states from which to sample. If empty, samples from all states. Defaults to an empty list.
        - exact_t_spacing (bool, optional): Determines if the sampling should occur at exact time points specified in `t` or at random times within the range defined by `t`. Defaults to False.
    
        Returns:
        - A DataFrame containing the sampled observations, structured to include the sampled times, states, and their frequencies.
        """
        return sample(self, t=t, n=n, states=states, exact_t_spacing=exact_t_spacing)
    
    def plot(self, t=np.arange(0, 50), atol=1e-4, rtol=1e-4):
        """
        Plots the state probabilities over time.
    
        Parameters:
        - t (array-like): Times at which to predict state probabilities, defaulting to a range from 0 to 50.
        - atol (float): Absolute tolerance for the numerical solver, default is 1e-4.
        - rtol (float): Relative tolerance for the numerical solver, default is 1e-4.
    
        Returns:
        - None: This method generates a plot but does not return any value.
        """
        p = self.predict(t=t, atol=atol, rtol=rtol)
        plt.plot(p.index, p, label=self.MCObj.states)
        plt.xlabel('Time')
        plt.ylabel('State probability')
        plt.grid()

    def save(self, var=None, filename='variable', path=''):
        """
        Saves 'var' to a file using dill in the specified path.

        Args:
            var: The variable (typically a DataFrame) to save.
            filename (str, optional): The name of the file to save the variable to. Defaults to 'dataframe.dill'.
            path (str, optional): The directory path where the file will be saved. Defaults to the current directory.
        """
        if not var:
            var = self # --> save the whole object
            
        filename = filename + '.dill'
        full_path = os.path.join(path, filename)
        if not os.path.exists(path):
            os.makedirs(path)
        with open(full_path, 'wb') as file:
            dill.dump(var, file)
        print(f"Variable saved to {full_path}.")

    def load(self, filename='variable', path=''):
        """
        Loads a DataFrame from a file using dill from the specified path.

        Args:
            filename (str, optional): The name of the file to load the DataFrame from. Defaults to 'dataframe.dill'.
            path (str, optional): The directory path from where the file will be loaded. Defaults to the current directory.
        """
        filename = filename + '.dill'
        full_path = os.path.join(path, filename)
        try:
            with open(full_path, 'rb') as file:
                model = dill.load(file)
            
            # Update relevant parameters by preserving the latest object properties:
            self.MCObj.x  = model.MCObj.x
            self.MCObj.s0 = model.MCObj.s0
            self.df       = model.df
            
            print(f"DataFrame loaded from {full_path}.")
        except FileNotFoundError:
            print(f"No file found with the name {full_path}.")
            

    
if __name__ == "__main__":
    model = InhomogeneousTimeMarkovChain()
    