#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 12:06:27 2024

@author: lisandro
"""
import numpy as np
import pandas as pd
from aux_MarkovChainCalibrationAlgorithms import SLSQP, Nelder_Mead, trust_constr, differential_evolution
from aux_functions import comp_error, getFrequencyTable

class MarkovChainCalibration:
    """
    Calibrate parameters for a Markov Chain model.
    
    This class implements methods for calibrating the parameters of a Markov Chain model
    using optimization techniques. It includes a cost function based on an error metric,
    a method to fit the model parameters to given data, and a method to compute the log likelihood
    for a given set of predictions.
    
    Attributes:
    - df: DataFrame containing the data to calibrate the Markov Chain.
    - MCObj: An object representing the Markov Chain, which must provide methods for prediction,
             parameter retrieval, and contain attributes for initial parameters, bounds, and constraints.
    """
    
    def __init__(self, df, MCObj,**kwargs):
        """
        Initialize the MarkovChainCalibration class with data and a Markov Chain object.
        
        Parameters:
        - df: A pandas DataFrame containing the data for calibration.
        - MCObj: An object representing the Markov Chain to be calibrated.
        """
        self._ = MCObj
        self.metric = kwargs.get('metric','LogL')
        self.load_ref_data(df)
        
    def load_ref_data(self, df):
        """
        Load reference data into the object's dataframe based on the specified metric.
    
        If the metric is set to 'RMSE', the object's dataframe is set to the dataframe
        stored in the object's `_` attribute's `df_freq` attribute. Otherwise, the 
        object's dataframe is set to the input `df`.
    
        Parameters:
        - df (pd.DataFrame): The dataframe to be loaded if the metric is not 'RMSE'.
        """
        if self.metric in ['RMSE','SE','MSE']:
            if self._.df_freq.empty:
                self.df = getFrequencyTable(y=df,states=self._.MCObj.states)
            else:
                self.df = self._.df_freq
                
        elif self.metric == 'LogL':
            self.df = df
        
    def cost_function(self,x,metric):
        """
        Define the cost function for optimization.
        
        This function calculates the cost based on the difference between predicted and observed
        values using a specified error metric. Currently supports log likelihood ('LogL') for
        'ihtmc' (inhomogeneous time Markov chain) type only.
        
        Parameters:
        - x: The parameter vector for which the cost is calculated.
        - df: A pandas DataFrame containing the data.
        - MCObj: The Markov Chain object.
        - error_metric: A string specifying the error metric to be used ('LogL').
        
        Returns:
        - The calculated cost.
        
        Raises:
        - ValueError: If the error metric is 'LogL' and the MC type is 'dtmc', or if an unsupported
                      error metric is provided.
        """
        yp, success = self._.predict(params=self._.MCObj.getMCParametersFromX(x), 
                                     t=self.df.index.unique(), 
                                     ConverenceDetails=True)
        if success:
            return self.compute_error(yp=yp,metric=metric)
        else:
            return 1E100 #--> Penalty due to bad set of parameters.
        
    def fit(self, **kwargs):
        """
        Calibrates a Markov Chain model using the specified parameters.
    
        This function takes any number of keyword arguments which are used
        for the calibration of the Markov Chain. It then applies the 
        MarkovChainCalibrationAlgorithm with these parameters to calibrate
        the model.
    
        Parameters:
        **kwargs: Various keyword arguments that are passed to the 
                  MarkovChainCalibrationAlgorithm for the calibration process.
    
        Returns:
        The result of the MarkovChainCalibrationAlgorithm after applying 
        the calibration process with the given parameters.
        """
        return self.MarkovChainCalibrationAlgorithm(**kwargs)
    
    def compute_error(self, yp, metric='LogL'):
        """
        Computes the error between model predictions and actual data using a specified metric.
    
        Parameters:
        yp (array-like): Predicted values.
        metric (str): The metric to use for error computation. Defaults to 'LogL' for log-likelihood.
    
        Returns:
        float: The computed error value.
        """
        return comp_error(y=self.df, yp=yp, metric=metric, states=self._.MCObj.states)
    
    def MarkovChainCalibrationAlgorithm(self,**kwargs):
        """
        Implements various optimization algorithms for model calibration.
    
        Parameters:
        opt_method (str): The optimization method to use.
        args (dict): Additional arguments specific to the optimization method.
    
        Returns:
        object: The result of the optimization process.
    
        Raises:
        ValueError: If an unsupported optimization method is specified.
        """
        opt_method =  kwargs.get('opt_method', 'differential_evolution')
        if opt_method == 'SLSQP':
            return SLSQP(self, **kwargs)
        elif opt_method == 'trust-constr':
            return trust_constr(self, **kwargs) 
        elif opt_method == 'Nelder-Mead':
            return Nelder_Mead(self, **kwargs) 
        elif opt_method == 'differential_evolution':
            return differential_evolution(self, **kwargs) 
        else:
            raise ValueError(f"Unsupported optimization method: {opt_method}")
    
            
            


    