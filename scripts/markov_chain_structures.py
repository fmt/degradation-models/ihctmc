class MarkovChainStructure:
    """
    A class to define the structure of a Markov Chain.

    Attributes:
    -----------
    MCType : str
        The type of Markov Chain.
    states : list
        A list of states in the Markov Chain.
    name : str
        The name of the specific Markov Chain structure.
    function : function
        A function associated with the Markov Chain.
    calibration_info : dict
        A dictionary to store calibration information.

    Methods:
    --------
    __init__(self, MCType, states, name, function):
        Initializes the MarkovChainStructure with the given parameters.
    """

    def __init__(self,MCStructureID,function,MCType):
        """
        Initialize the MarkovChainStructure with the specified type, states, name, and function.

        Parameters:
        -----------
        MCType : str
            The type of Markov Chain.
        states : list
            A list of states in the Markov Chain.
        name : str
            The name of the specific Markov Chain structure.
        function : function
            A function associated with the Markov Chain.
        """
        
        # Dynamic class assignment based on specified Markov Chain structure.
        if MCStructureID == 'ihtmc_s6_typeA':
            from markov_chain_types.ihtmc_s6_typeA import MC
        elif MCStructureID == 'ihtmc_s6_typeB':
            from markov_chain_types.ihtmc_s6_typeB import MC
        elif MCStructureID == 'ihtmc_s2_typeA':
            from markov_chain_types.ihtmc_s2_typeA import MC
        elif MCStructureID == 's5_typeA':
            from markov_chain_types.s5_typeA import MC
        elif MCStructureID == 's6_typeA':
            from markov_chain_types.s6_typeA import MC
        else:
            raise ValueError("Specified Markov Chain structure does not match predefined structures.")
            
        # Dynamically assign this instance's class to the specified MC class
        self.__class__ = MC
        # Initialize the new class with the function
        self.__init__(function,MCType)