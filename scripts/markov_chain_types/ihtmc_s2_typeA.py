#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 16:02:21 2024

@author: lisandro
"""
import numpy as np
import pandas as pd
from aux_functions import transition_rate, random_mass_function

class MC:
    """Markov Chain model for managing transitions and rates."""
    
    def __init__(self,function):
        
        self.MCType = 'ihtmc'
        self.MCid = 'ihtmc_s2_typeA'
        self.function = function
        self.states = [1,2]
        self.calibrated = False
        self.convergence_info = dict()
        self.genInitalGuessParameters()
    
    def number_parameters(self,function):
        if function == 'exponential':
            return 1
        else:
            return 2
            
    def transitions(self) -> pd.DataFrame:
        """Fetches the transition matrix for the Markov chain.
        
        Markov Chain Diagram:
        ```
        1 --> 2 
        ```
        
        Returns:
            Transition matrix as a pandas DataFrame.
        """
        data = np.array([[1, 1],
                         [0, 1]])
        return pd.DataFrame(data, index=np.array(self.states), columns=np.array(self.states))
    
    def Q(self, t: float, x: float) -> np.ndarray:
        """Computes the transition rate matrix Q for a given time.
        
        Parameters:
            t: Time to evaluate rates.
            function: Probability density function, defaults to 'gompertz'.
        
        Returns:
            Transition rate matrix Q.
        """        
        if self.function != 'exponential':
            param = {'a': [x[0]], 'b': [x[1]]}
        else:
            param = {'a': x}

        Q = np.array([
            [-transition_rate(0, t, param, self.function), transition_rate(0, t, param, self.function)],
            [0, 0]
        ])

        return Q

    def genInitalGuessParameters(self) -> np.ndarray:
        """Load default initial parameter vector based on function.
        
        Parameters:
            function: Name of the function to use.
        
        Returns:
            Initial parameter vector.
        """
        
        self.x = np.random.uniform(1E-18,50, self.number_parameters(self.function))
        self.s0 = np.array([1.0,0.0])#random_mass_function(len(self.states))

    def bounds(self) -> list:
        """Defines bounds for optimization based on function and state.
        
        Parameters:
            function: Name of the function to use.
            state: Current state of the system.
        
        Returns:
            Bounds for optimization parameters.
        """
        return [(1E-18, 50)]*self.number_parameters(self.function) + [(0, 1)] * len(self.states)
                    
    def getData(self, x=None):
        """
        Prepare data for optimization process.
    
        If 'x' is not provided, returns a dictionary with 's0' and 'x' from the instance.
        If 'x' is provided, 's0' is set to the last two elements of 'x', and 'x' to the rest.
    
        Parameters:
        - x (list, optional): The input data to process. Defaults to None.
    
        Returns:
        dict: A dictionary with keys 's0' and 'x'. 's0' is either the instance's s0 or the last two elements of 'x'. 'x' is either the instance's x or 'x' without its last two elements.
        """
        return {'s0': self.s0 if x is None else x[-2:], 'x': self.x if x is None else x[:-2]}

    
    def constraints(self) -> list:
        """Defines constraints for optimization ensuring the last N numbers of the array sum to 1.0.
        
        Args:
            N: The number of last elements in the array to sum to 1.0.
            
        Returns:
            A list containing the constraint.
        """
        return [{'type': 'eq', 'fun': lambda x: 1.0 - sum(x[-self.number_parameters(self.function):])}]