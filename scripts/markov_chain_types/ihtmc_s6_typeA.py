import numpy as np
import pandas as pd
from aux_functions import transition_rate

class MC:
    """Markov Chain model for managing transitions and rates."""
    
    def __init__(self):
        pass
    
    def transitions(self) -> pd.DataFrame:
        """Fetches the transition matrix for the Markov chain.
        
        Markov Chain Diagram:
        ```
        1 --> 2 --> 3 --> 4 --> 5 --> 6
        |                              ^
        |                              |
        +------------------------------+
               |                       |
               v                       |
               2 ----------------------+
                       |               |
                       v               |
                       3 --------------+
                               |       |
                               v       |
                               4 ------+
        ```
        
        Returns:
            Transition matrix as a pandas DataFrame.
        """
        data = np.array([[1, 1, 0, 0, 0, 1],
                         [0, 1, 1, 0, 0, 1],
                         [0, 0, 1, 1, 0, 1],
                         [0, 0, 0, 1, 1, 1],
                         [0, 0, 0, 0, 1, 1],
                         [0, 0, 0, 0, 0, 1]])
        return pd.DataFrame(data, index=np.array([1,2,3,4,5,6]), columns=np.array([1,2,3,4,5,6]))
    
    def Q(self, t: float, x: float, function: str = 'gompertz') -> np.ndarray:
        """Computes the transition rate matrix Q for a given time.
        
        Parameters:
            t: Time to evaluate rates.
            function: Probability density function, defaults to 'gompertz'.
        
        Returns:
            Transition rate matrix Q.
        """        
        if function != 'exponential':
            param = {'a': x[0:9], 'b': x[9:]}
        else:
            param = {'a': x}

        Q = np.array([
            [-transition_rate(0, t, param, function) - transition_rate(4, t, param, function), transition_rate(0, t, param, function), 0, 0, 0, transition_rate(4, t, param, function)],
            [0, -transition_rate(1, t, param, function) - transition_rate(5, t, param, function), transition_rate(1, t, param, function), 0, 0, transition_rate(5, t, param, function)],
            [0, 0, -transition_rate(2, t, param, function) - transition_rate(6, t, param, function), transition_rate(2, t, param, function), 0, transition_rate(6, t, param, function)],
            [0, 0, 0, -transition_rate(3, t, param, function) - transition_rate(7, t, param, function), transition_rate(3, t, param, function), transition_rate(7, t, param, function)],
            [0, 0, 0, 0, -transition_rate(8, t, param, function), transition_rate(8, t, param, function)],
            [0, 0, 0, 0, 0, 0]
        ])

        return Q

    def parameters(self, function: str) -> np.ndarray:
        """Load default initial parameter vector based on function.
        
        Parameters:
            function: Name of the function to use.
        
        Returns:
            Initial parameter vector.
        """
        if function == 'exponential':
            return np.concatenate((0.05*np.ones(9), np.array([1, 0, 0, 0, 0, 0])))
        return np.ones(18), np.array([1, 0, 0, 0, 0, 0])

    def bounds(self, function: str) -> list:
        """Defines bounds for optimization based on function and state.
        
        Parameters:
            function: Name of the function to use.
            state: Current state of the system.
        
        Returns:
            Bounds for optimization parameters.
        """
        state = [1,2,3,4,5,6]
        if function == 'exponential':
            return [(1E-18, 2)] * 9 + [(0, 1)] * len(state)
        return [(1E-18, 100)] * 18 + [(0, 1)] * len(state)
                
    def constraints(self) -> list:
        """Defines constraints for optimization.
        
        Returns:
            Constraints for optimization.
        """
        return [{'type': 'eq', 'fun': lambda x: 1 - sum(x[-6:])}]
    
    def getData(self,x):
        '''
        Prepare data for optimization process
        '''
        return {'s0':x[-6:],
                'x':x[0:-6]}