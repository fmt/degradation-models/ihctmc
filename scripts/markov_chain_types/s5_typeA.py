import numpy as np
import pandas as pd
from aux_functions import transition_rate, random_mass_function
from scipy.special import softmax
from aux_common_functions_markov_chain_types import getInitalParameters, bounds_x, bounds_s0, Transform, InverseTransform

class MC:
    """Markov Chain model for managing transitions and rates."""
    
    def __init__(self,function,MCType='ihtmc'):
        
        self.MCType = MCType
        self.MCid = 's5_typeA'
        self.function = function
        self.states = [1,2,3,4,5]
        self.calibrated = False
        self.convergence_info = dict()
        self.getInitalParameters()
        self.bounds = self.getBounds()
    
    def number_parameters(self):
        if self.function == 'exponential' or self.MCType == 'dtmc':
            return 4
        else:
            return 2*4
            
    def transitions(self) -> pd.DataFrame:
        """Fetches the transition matrix for the Markov chain.
        
        Markov Chain Diagram:
        ```
        1 --> 2 --> 3 --> 4 --> 5
        ```
        
        Returns:
            Transition matrix as a pandas DataFrame.
        """
        data = np.array([[1, 1, 0, 0, 0],
                         [0, 1, 1, 0, 0],
                         [0, 0, 1, 1, 0],
                         [0, 0, 0, 1, 1],
                         [0, 0, 0, 0, 1],
                         ])
        return pd.DataFrame(data, index=np.array(self.states), columns=np.array(self.states))
        
    def P(self, x=None, output_format='array'):
        """
        Computes a transition probability matrix based on given probabilities.
    
        The function creates a square matrix of zeros with dimensions equal to the number of states.
        Each element x[i] in the input 'x' is used to set the transition probability from state i to state i (P[i, i]),
        and the transition probability from state i to state i+1 (P[i, i+1]) is set to 1 - x[i].
        The last state's transition probability to itself is set to 1, indicating a terminal state.
    
        Parameters:
        - x (list, optional): A list of probabilities for transitioning from one state to the next. 
                              If None, uses the instance's x attribute.
        - output_format (str, optional): The format of the output. Defaults to 'array'.
    
        Returns:
        - numpy.ndarray: A 2D numpy array representing the transition probability matrix.
        """
        if not x:
            x = self.x
        P = np.zeros((len(self.states), len(self.states)))        
        for i in range(4):
            P[i, i] = x[i]
            P[i, i+1] = 1 - x[i]
        P[4, 4] = 1
        return P
        
    def Q(self, t: np.ndarray, x: float, output_format='array') -> np.ndarray:
        """
        Computes the transition rate matrix (Q) for a given time and parameters.
    
        Parameters:
        - t (np.ndarray): An array of time points at which the transition rates are to be computed.
        - x (float): The parameter(s) for the transition rate function. If the function is exponential, 'x' is a single float.
          Otherwise, 'x' is expected to be an array where the last 4 elements are treated as 'b' parameters and the rest as 'a' parameters.
        - output_format (str, optional): The format of the output. If 'array', the function returns a numpy array. If 'dataframe',
          the function returns a pandas DataFrame. Defaults to 'array'.
    
        Returns:
        - np.ndarray or pd.DataFrame: The transition rate matrix (Q) as a numpy array or pandas DataFrame, depending on the output_format.
          The matrix dimensions are (len(t), len(self.states), len(self.states)) for 'array' format, and the DataFrame is reshaped 
          accordingly with multi-index columns for each state transition and indexed by time points.
    
        Note:
        - The function is designed to work with specific transition rate functions and expects the class instance to have 'states' 
          and 'function' attributes, with 'function' determining the calculation method.
        - The diagonal elements of Q represent the negative transition rates out of each state, and the off-diagonal elements represent
          the transition rates into each state from each other state.
        """
        if self.function != 'exponential':
            param = {'a': x[:-4], 'b': x[-4:]}
        else:
            param = {'a': x}
    
        if isinstance(t, float):
            t = np.array([t])
    
        Q = np.zeros((len(t), len(self.states), len(self.states)))
    
        for i in range(len(self.states)):
            Q[:, i, i] = -transition_rate(i, t, param, self.function)
    
        # Setting off-diagonal transition rates
        Q[:, 0, 1] = -Q[:, 0, 0]
        Q[:, 1, 2] = -Q[:, 1, 1]
        Q[:, 2, 3] = -Q[:, 2, 2]
        
        if (np.sum(Q,axis=2)>1E-6).any(): 
            raise ValueError('The sum of rows is larger than 1E-6.')

        if output_format == 'array':
            return Q
        elif output_format == 'dataframe':
            return pd.DataFrame(Q.reshape(len(t), Q.shape[1]**2),
                                columns=[(from_state, to_state) for from_state in self.states for to_state in self.states],
                                index=t)
    
    def getInitalParameters(self) -> np.ndarray:
        """
        Retrieves initial parameters for the current instance.
    
        This method calls the global function `getInitalParameters`, passing `self` as an argument,
        to obtain initial parameters. It updates the instance with these parameters.
    
        Returns:
            np.ndarray: The `x` parameter obtained from `getInitalParameters` function, intended to be used as initial parameters.
    
        Note:
            This method also updates `self.x` and `self.s0` with the values obtained from the `getInitalParameters` function.
        """
        x, s0 = getInitalParameters(self)
        self.x = x
        self.s0 = s0

    def getBounds(self) -> list:
        """
        Combines and returns the bounds for x and s0 as a single list.
    
        Returns:
            list: A list containing the bounds for x and s0.
        """
        return bounds_x(self) + bounds_s0(self)
    

    def getMCParametersFromX(self, x):
        """
        Reparametrizes the Markov Chain with a new set of parameters based on the input `x`.
    
        This method constructs a new parameter dictionary with 's0' being the last five elements of `x`
        and 'x' being all other elements before the last five. It then applies these parameters
        to the current instance using the `Transform` method.
    
        Args:
            x (iterable): An array-like object containing parameters for reparametrization. The last five elements
                          are assigned to 's0', and the rest to 'x'.
    
        Returns:
            The result of the `Transform` method called with the current instance and the new parameters.
    
        Note:
            The `Transform` method is assumed to be a method of the current class or a globally accessible function
            that accepts the instance and a parameter dictionary to perform some transformation or update.
        """
        return Transform(self, param = {'s0':x[-5:],'x':x[:-5]})
    
    def getXFromMCParameters(self, x=None, s0=None):
        """
        Calculates and returns the inverse transform based on Monte Carlo parameters.
    
        This method computes the inverse transform using the provided `x` and `s0` values. If `x` or `s0` are not provided,
        it defaults to using the object's `x` and `s0` attributes, respectively.
    
        Parameters:
        - x (Optional): The value to be used in the inverse transform calculation. Defaults to the object's `x` attribute if not provided.
        - s0 (Optional): The starting value to be used in the inverse transform calculation. Defaults to the object's `s0` attribute if not provided.
    
        Returns:
        - The result of the InverseTransform function, utilizing the provided or default `x` and `s0` values.
        """
        if not x:
            x = self.x
        if not s0:
            s0 = self.s0
        return InverseTransform(self, x, s0)

    def getMCParameters(self, x=None):
        """
        Retrieves parameters 's0' and 'x' based on the provided input.
    
        If 'x' is not provided, 's0' and 'x' are retrieved from the object's attributes.
        If 'x' is provided, 's0' is set to the last 5 elements of 'x', and 'x' is set to the rest.
    
        Parameters:
        x (optional): Array-like or None. If provided, it is used to determine 's0' and 'x'.
    
        Returns:
        dict: A dictionary containing 's0' and 'x' parameters.
        """
        return {'s0': self.s0 if x is None else x[-5:], 'x': self.x if x is None else x[:-5]}

    def constraints(self) -> list:
        """Defines constraints for optimization ensuring the last N numbers of the array sum to 1.0.
        
        Args:
            N: The number of last elements in the array to sum to 1.0.
            
        Returns:
            A list containing the constraint.
        """
        return [{'type': 'eq', 'fun': lambda x: 1.0 - sum(x[-self.number_parameters(self.function):])}]
    
    
    