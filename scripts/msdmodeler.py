#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 16:14:09 2024
@author: Lisandro A. Jimenez-Roa
"""

import dill
import pandas as pd
import numpy as np
import random
import string
from turnbull_estimator import TurnbullEstimator
from ihtmc import InhomogeneousTimeMarkovChain as IHTMC

class MultiStateDegradationModeler:
    
    """
    Main degradation modeller for multi-state degradation.
    
    This section can include a more detailed description of the class, explaining its purpose and how it should be used. You can also mention any important notes or warnings regarding the class.
    
    Attributes:
        system_features (pd.DataFrame): Contains a dataframe with all the properties of the system, each row represents a system component. Each column is a property e.g., location, length, pipe_id.
    
    Args:
        case_name (str): Name of the case study to load from [file_name].
        db_path (str): Path of the db. file.
        context_var_blacklist (Boolean): If True, removes predefined variables from the context. If False, it loads all the variables as context.
        verbose(int): If 2: Prints all the information when building the object.
    
    Examples:
        >>> instance = MyClass(param1, param2)
        >>> instance.method1()
        Expected result from method1.
    
    
    """
    def __init__(self,system_features,system_inspections,verbose=2):
        
        # load default parameters:
        self.default_parameters = self.get_default_parameters({k: v for k, v in locals().items() if k != 'self'})
        
        # Load data:
        self.system_features = system_features
        self.system_inspections = system_inspections
        self.df_msdm = pd.DataFrame()
        
        
    def check_msdm_avail(self,df,feat):
        """
        Check whether all components have associated at least one degradation model.
    
        Parameters:
        - df (pd.DataFrame): contains all the components features.
        - feat (pd.DataFrame): contains the features associated to a degradation model
    
        Returns:
        - pd.DataFrame: DataFrame with Boolean values indicating whether exists (1) or not(0) a degradation model.
        """
        
        isThereMSDM = pd.DataFrame()
        
        if not self.df_msdm.empty:
            raise ValueError('Program this part')
        else:
            isThereMSDM = pd.DataFrame({'isThereMSDM':list(np.zeros(df.shape[0]).astype(bool))})
        
        return isThereMSDM
        
    def fit(self,case_to_train,context_var_for_msdm,save_path=None,verbose=1):
        """
        Main manager to fit Multi-State Degradation Models (MSDM)
    
        Parameters:
        - case_to_train (df): DataFrame that containts the contextual values and additional parameters to train the MSDM.
        - context_var_for_msdm (list): Indicates the contextual variables used to build the degradation model.
        - save_path (str): path where the degradation model is saved.
    
        Returns:
        - object: object associated to the degradation model.
        """
        for index,row in case_to_train.iterrows():
            # Fetch sub-set from self.system_features:
            subset_system_features = self.system_features[self.system_features[context_var_for_msdm].eq(pd.Series(row[context_var_for_msdm])).all(axis=1)]
            # Fetch sub-set from self.system_inspections:
            subset_system_inspections = self.get_lifetime_data(subset_system_features=subset_system_features,damage_code=row.code,add_collapse=True,flag='over_length')
            # Train Turnbull estimator:
            if False:
                TE = TurnbullEstimator(df=subset_system_inspections)
                TE.fit(verbose=verbose)
            if row.markov_chain_type == 'IHCTMC':
                # Train Inhomogeneous-continuous time Markov chain
                ihtmc = IHTMC(df=subset_system_inspections)
                ihtmc.fit()
            
            # Generate an unique identifier to the model:
            unique_id = self.generate_random_string()
        
        if save_path:
            # Save model externally
            pass 
        
    def get_lifetime_data(self, subset_system_features, damage_code, severities=[1,2,3,4,5,6], flag='most_critical', add_collapse=False, cut_max_age=None, cut_max_age_flag='low', location=None):
        df = self.system_inspections[self.system_inspections['pipe_id'].isin(subset_system_features['pipe_id'])]
        df = df.merge(self.system_features[['pipe_id', 'construction_year', 'cohort', 'length', 'width']], on='pipe_id')
        df['inspection_date'] = pd.to_datetime(df['inspection_date'])
        df['construction_year'] = pd.to_datetime(df['construction_year'].astype(int).astype(str) + '-01-01')
        df['pipe_age_during_inspection'] = (df['inspection_date'] - df['construction_year']).dt.total_seconds() / (365.25 * 24 * 3600)
        df = df[df['pipe_age_during_inspection'] >= 0]
        if cut_max_age:
            df = df[df['pipe_age_during_inspection'] <= cut_max_age] if cut_max_age_flag == 'low' else df[df['pipe_age_during_inspection'] > cut_max_age]
        if location:
            df = df[df['location'] == location]
        non_matching_codes = df['code'] != damage_code
        df.loc[non_matching_codes, ['code', 'damage_class']] = [damage_code, 1]
        if add_collapse:
            collapse_indices =  self.system_inspections['code'] == 'BAC'
            df.loc[collapse_indices, 'damage_class'] = 6
        if flag == 'most_critical':
            df = df.groupby('inspection_id').agg({'pipe_id': 'first', 'pipe_age_during_inspection': 'first', 'damage_class': 'max'}).reset_index()
        elif flag ==  'over_length':
            df = df[['pipe_id', 'pipe_age_during_inspection', 'damage_class']]
        else:
            raise ValueError('Unknown flag')
        df.rename(columns={'damage_class': 'severity'}, inplace=True)
        return df.sort_values(by='pipe_age_during_inspection').reset_index(drop=True)

    def generate_random_string(self,N=30):
        return ''.join(random.choices(string.ascii_letters + string.digits, k=N))
    
    def get_default_parameters(self,param):
        """
        Fetch default parameters:
    
        Parameters:
        - 
    
        Returns:
        - dict: With default parameters.
        """
        
        del param['system_features']
        del param['system_inspections']

        return param
        
if __name__ == "__main__":
    #system_structure = MultiStateDegradationModeler()
    pass
    