#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 11:53:51 2024

@author: lisandro
"""
import numpy as np
import scipy.stats as stats

# Gompertz distribution:
class gompertz:
    def __init__(self):
        pass
    def hazard_rate(t, alpha, beta):
        return alpha * beta * np.exp(beta * t) #stats.gompertz.pdf(t, alpha, scale=beta)/(1-stats.gompertz.cdf(t, alpha, scale=beta))#
    def pdf(t, alpha, beta):
        return alpha * beta * np.exp(alpha) * np.exp(beta * t) * np.exp(-alpha * np.exp(beta * t))
    def cdf(t, alpha, beta):
        return 1 - np.exp(-alpha * np.exp(beta * t) + alpha)
    
# Gamma distribution:
class gamma:
    def __init__(self):
        pass
    def hazard_rate(t, alpha, beta):
        pdf = stats.gamma.pdf(t, alpha, scale=1/beta)
        survival_function = 1 - stats.gamma.cdf(t, alpha, scale=1/beta)
        hazard_rate = pdf / survival_function
        return hazard_rate
    def pdf(t, alpha, beta):
        return stats.gamma.pdf(t, alpha, scale=1/beta)
    def cdf(t, alpha, beta):
        return 1 - stats.gamma.cdf(t, alpha, scale=1/beta)
    
# Log-logistic distribution:
class loglogistic:
    def __init__(self):
        pass
    def hazard_rate(t, alpha, beta):
        #numerator = (beta / alpha) * ((t / alpha)**(beta - 1))
        #denominator = (1 + (t / alpha)**beta)**2
        #pdf =  numerator / denominator
        #log_transform = np.log(t / alpha) * beta
        #cdf = stats.logistic.cdf(log_transform)
        return ((beta / alpha) * (t / alpha)**(beta - 1)) / (1 + (t / alpha)**beta) #pdf / (1 - cdf)
    def pdf(t, alpha, beta):
        numerator = (beta / alpha) * ((t / alpha)**(beta - 1))
        denominator = (1 + (t / alpha)**beta)**2
        return numerator / denominator
    def cdf(t, alpha, beta):
        # Applying the log transformation to t and then using the logistic CDF
        log_transform = np.log(t / alpha) * beta
        return stats.logistic.cdf(log_transform)
    
class lognormal:
    def __init__(self):
        pass
    def hazard_rate(t, alpha, beta):
        pdf = stats.lognorm.pdf(t, alpha, scale=np.exp(beta))
        sf = 1 - stats.lognorm.cdf(t, alpha, scale=np.exp(beta))
        return pdf / sf
    def pdf(t, alpha, beta):
        return stats.lognorm.pdf(t, alpha, scale=np.exp(beta))
    def cdf(t, alpha, beta):
        return stats.lognorm.cdf(t, alpha, scale=np.exp(beta))
    
# Exponential distribution:
class exponential:
    def __init__(self):
        pass
    def hazard_rate(t,lambda_):
        if isinstance(t,float):
            return lambda_
        elif isinstance(t,np.ndarray) or isinstance(t,list):
            return lambda_*np.ones(len(t))
        
# Weibul distribution:
class weibull:
    def __init__(self):
        pass
    def hazard_rate(t, alpha, beta):
        return  (alpha / beta) * (t / beta)**(alpha - 1)#(beta / alpha) * (t / alpha)**(beta - 1) # 
    def pdf(t, alpha, beta):
        return (beta/alpha)*((t/alpha)**(beta-1))*np.exp(-(t/alpha)**beta)
    def cdf(t, alpha, beta):
        return 1 - np.exp(-(t/alpha)**beta)