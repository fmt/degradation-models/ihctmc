#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 15:22:52 2024

@author: lisandro
"""
from ihtmc import InhomogeneousTimeMarkovChain as IHTMC
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

ihtmc = IHTMC(MCStructureID='ihtmc_s2_typeA')
params = {'s0': np.array([0.9, 0.1]),
          'x': np.array([1,
                         0.1])}
p = ihtmc.predict(params=params,t=np.linspace(0, 50,1000),atol=1e-4, rtol=1e-4)  # Removed colon at the end
#%% Randomly sample from Markov chain --> Generate synthetic dataset.
num_inspections = 1000
ti = np.array(p.index)
s = np.array([int(i[2:]) for i in p.columns])
times = np.random.uniform(0, 50, num_inspections)
choices = [np.random.choice(s, p=p.iloc[np.argmin(np.abs(t - ti))].values) for t in times]
obs = np.column_stack((times, choices))
subset_system_inspections = pd.DataFrame(obs, columns=['time', 'state']).sort_values(by='time', ascending=True).reset_index(drop=True)
subset_system_inspections['state'] = subset_system_inspections['state'].astype(int)
subset_system_inspections.set_index('time', inplace=True)
#%% Let's now infer the parameters of the dataset.
gompertz = IHTMC(df=subset_system_inspections,MCStructureID='ihtmc_s2_typeA')
gompertz.fit()
#%% 
plt.close('all')
plt.plot(p.index,p)
gompertz.plot()
