import pandas as pd
import numpy as np
from lifelines import KaplanMeierFitter as KM
import time
from datetime import datetime
import dill

class TurnbullEstimator:
    '''
    Doubly-Censored Kaplan Meier estimator.
    
    Attributes:
        df (pandas.DataFrame): Dataframe containing the data to fit the model.
        states (list, optional): List of states to consider for the estimation. Defaults to None, in which case it uses unique states from the dataframe.
    '''
    def __init__(self, df, states=None):
        '''
        Initializes the TurnbullEstimator with a dataframe and optionally a list of states.
        
        Parameters:
            df (pandas.DataFrame): Dataframe containing the data.
            states (list, optional): Specific states to use for the estimation.
        '''
        self.df = df
        self.states = states
        self.fit()

    def fit(self):
        '''
        Fits the model using the data provided during initialization.
        
        This method processes the dataframe to handle doubly-censored data and fits a Kaplan Meier model to each state.

        Returns:
            pandas.DataFrame: A dataframe summarizing the model parameters for each state.
        '''
        if not self.states:
            self.states = list(self.df['state'].unique())
            
        # Prepare df table to be handled by the turnbull estimator:
        df = self.df.reset_index()  # Move 'time' from index to a column
        df = df.loc[df.index.repeat(df['count'])].drop(columns=['count']).reset_index(drop=True)

        models_summary = []
        for s in self.states:
            print("Binarization threshold: "+str(s))
            dfp = pd.DataFrame({
                'left': np.nan,
                'right': np.nan
                }, index=df.index)
            
            # The transition did not occur:
            dfp.loc[df['state'] <= s, 'right'] = np.inf
            dfp.loc[df['state'] <= s, 'left'] = df.loc[df['state'] <= s]['time']
            
            # If the transition occurred:
            dfp.loc[df['state'] > s, 'right'] = df.loc[df['state'] > s]['time']
            dfp.loc[df['state'] > s, 'left'] = 0
            
            start_time = time.time()
            km_model = KM().fit_interval_censoring(dfp['left'], dfp['right'])
            end_time = time.time()
            
            # Saving model parameters:
            model_summary = {
                'date_time': datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                'convergence_time_sec': end_time - start_time,
                'binary_threshold': s,
                'model': km_model,
            }
            models_summary.append(model_summary)
        
        self.model = pd.DataFrame(models_summary)
        return self.model
    
    def predict(self, t=np.arange(0, 50), km_mean=True):
        """
        Predicts the non-parametric maximum likelihood estimate (NPMLE) for a given range of time points and binary states.
        
        This function iterates through pairs of consecutive states to calculate the upper and lower NPMLE estimates.
        For the first state, it directly uses the model's predictions. For subsequent states, it calculates the difference
        in predictions between the current state and the previous state. Optionally, it can compute the mean of the upper
        and lower estimates to simplify the output to a single value per state.
    
        Parameters:
        - t (np.ndarray, optional): An array of time points for which predictions are to be made. Defaults to np.arange(0, 50).
        - km_mean (bool, optional): A flag to determine if the mean of the upper and lower NPMLE estimates should be computed.
                                    If True, the output is simplified to a single value per state. Defaults to True.
    
        Returns:
        - pd.DataFrame: A DataFrame containing the NPMLE estimates. If km_mean is True, each state is represented by a single
                        column with the mean value. Otherwise, each state has two columns representing the upper and lower
                        NPMLE estimates.
        """
        # Initialize an empty dictionary to store predictions
        p = dict()  # Assuming this initialization is done if not already
        for index, s in enumerate(self.states):
            # Handle the first state separately
            if s == self.states[0]:
                predictions = self.model[self.model['binary_threshold'] == s].iloc[0].model.predict(t)
                p['s_' + str(s) + '_up'] = np.array(predictions['NPMLE_estimate_upper'])
                p['s_' + str(s) + '_down'] = np.array(predictions['NPMLE_estimate_lower'])
            else:
                # For subsequent states, calculate the difference in predictions
                previous_s = self.states[index - 1]  # Get the previous state
                predictions_diff = self.model[self.model['binary_threshold'] == s].iloc[0].model.predict(t) - \
                                   self.model[self.model['binary_threshold'] == previous_s].iloc[0].model.predict(t)
                p['s_' + str(s) + '_up'] = predictions_diff['NPMLE_estimate_upper']
                p['s_' + str(s) + '_down'] = predictions_diff['NPMLE_estimate_lower']
        
        # Convert to DataFrame if needed outside this snippet
        p = pd.DataFrame(p)
            
        if km_mean:
            # If km_mean is True, compute the mean of the upper and lower estimates
            for i in self.states:
                prefix = f's_{i}'
                up_col = f'{prefix}_up'
                down_col = f'{prefix}_down'
                p[prefix] = p[[up_col, down_col]].mean(axis=1)
                p.drop(columns=[up_col, down_col], inplace=True)
            # Rename columns to match states
            p.columns = self.states
            
        # Replace NaN values by zerp
        p.iloc[0] = p.iloc[0].fillna(0) #--> only applicable to the first row.
    
        return p
                
    def save(self, file_name, file_location):
        with open(f"{file_location}/{file_name}.dill", "wb") as file:
            dill.dump(self, file)
     
if __name__ == "__main__":
    
    from copy import copy
    import matplotlib.pyplot as plt
    from aux_functions import getFrequencyTable
    from DataHandlingManager import DataHandlingManager
    args = {'system_database':'/Users/lisandro/Library/CloudStorage/OneDrive-UniversiteitTwente/PhD/Papers/datasets/raw/breda.db',
        'inspection_database':'/Users/lisandro/Library/CloudStorage/OneDrive-UniversiteitTwente/PhD/Papers/datasets/raw/breda.db'} 
    
    dh = DataHandlingManager(**args)
    df = dh.get_lifetime_data(code='BAF',cohort='CS',add_collapse=True)
    ft = getFrequencyTable(y=copy(df),states=[1,2,3,4,5,'F'],delta=3)
    df.loc[df['state'] == 'F','state'] = 6
    tb = TurnbullEstimator(df=df,states=[1,2,3,4,5,6])
    tb.model.loc[tb.model['binary_threshold'] == 6, 'binary_threshold'] = 'F'
    tb.states = [1,2,3,4,5,'F']

    fig, axs = plt.subplots(2, 3, sharex=True, sharey=True, figsize=(6, 4.5))
    axs = axs.flatten()
    t = np.arange(0,100)
    y = tb.predict(t=t)
    for cont, s in enumerate(tb.states):
        axs[cont].plot(t,y[s])
        axs[cont].scatter(ft.index, ft[s], s = ft['total_count']/20)

    tb.save(file_name='turbull_estimator_CS_BAF',
            file_location='/Users/lisandro/Library/CloudStorage/OneDrive-UniversiteitTwente/PhD/Papers/p06b_RL_ihtmc/paper/figures/')
    
    df = dh.get_lifetime_data(code='BAF',cohort='CMW',add_collapse=True)
    ft = getFrequencyTable(y=copy(df),states=[1,2,3,4,5,'F'],delta=3)
    df.loc[df['state'] == 'F','state'] = 6
    tb = TurnbullEstimator(df=df,states=[1,2,3,4,5,6])
    tb.model.loc[tb.model['binary_threshold'] == 6, 'binary_threshold'] = 'F'
    tb.states = [1,2,3,4,5,'F']
    
    fig, axs = plt.subplots(2, 3, sharex=True, sharey=True, figsize=(6, 4.5))
    axs = axs.flatten()
    t = np.arange(0,100)
    y = tb.predict(t=t)
    for cont, s in enumerate(tb.states):
        axs[cont].plot(t,y[s])
        axs[cont].scatter(ft.index, ft[s], s = ft['total_count']/20)

    tb.save(file_name='turbull_estimator_CMW_BAF',
            file_location='/Users/lisandro/Library/CloudStorage/OneDrive-UniversiteitTwente/PhD/Papers/p06b_RL_ihtmc/paper/figures/')