#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 21 10:03:49 2024

@author: lisandro
"""
import pandas as pd
from lifelines import KaplanMeierFitter as KM
import numpy as np
import matplotlib.pyplot as plt


class TurnbullEstimator:
    """
    A class to implement the Turnbull Estimator for interval-censored data analysis.
    
    This estimator is particularly useful for analyzing data where the exact value of the
    event time is not known, but it is known to occur within a certain interval. It is widely
    used in survival analysis, reliability engineering, and various fields of medical research.

    Attributes:
        df (pandas.DataFrame): The dataset containing the interval-censored data. Each row represents
            an observation, and it should include columns that define the lower and upper bounds of the
            censoring intervals for the event of interest.
        verbose (int): Controls the verbosity of the output during model fitting and prediction. A higher
            value means more detailed output. Default is 1.
        list_models (pandas.DataFrame): Stores the fitted model objects for each severity level. Each row
            corresponds to a severity level, and the columns include the model details and estimations.

    Args:
        df (pandas.DataFrame): The data frame with the data to be analyzed. It should include at least
            two columns indicating the lower and upper bounds of the censoring intervals.
        verbose (int, optional): Verbosity level. Higher values produce more detailed output during the
            model fitting process. Defaults to 1.

    Examples:
        Initialize a TurnbullEstimator object with a DataFrame containing interval-censored data:
        >>> estimator = TurnbullEstimator(df, verbose=1)
        Fit the estimator for specified severity levels:
        >>> estimator.fit(severities=[1, 2, 3, 4, 5, 6])
        Generate predictions and plot them:
        >>> estimator.predict(t=np.arange(0, 50), severities=[1, 2, 3, 4, 5, 6])
        >>> estimator.plot(t=np.arange(0, 50))
    """
    
    def __init__(self, df, verbose=1):
        """Constructor for TurnbullEstimator."""
        self.df = df
        self.verbose = verbose
        self.list_models = pd.DataFrame()

    def fit(self,severities=[1,2,3,4,5,6], verbose=1):
        """Fit the Turnbull estimator for each severity level in the given list.

        Args:
            severities (list, optional): List of severity levels to fit models for. Defaults to [1,2,3,4,5,6].
            verbose (int, optional): Verbosity level. Defaults to 1.
        """
        models = {'s>': [], 'turnbull_estimator': []}
        for s in severities:
            if verbose == 2:
                print(f'Current binarization threshold: s>{s}')
            
            dfp = pd.DataFrame({
                'left': np.where(self.df['severity'] <= s, self.df['pipe_age_during_inspection'], 0),
                'right': np.where(self.df['severity'] > s, self.df['pipe_age_during_inspection'], np.inf)
            })
            
            km_model = KM().fit_interval_censoring(dfp['left'], dfp['right'])
            models['s>'].append(s)
            models['turnbull_estimator'].append(km_model)
        
        self.list_models = pd.DataFrame(models)
        
    def predict(self, t=np.arange(0, 50), severities=[1, 2, 3, 4, 5, 6], km_mean=True):
        """
        Generates predictions for given time points and severities.

        Parameters:
        - t: numpy array of time points for which to predict. Default is np.arange(0, 50).
        - severities: list of severity levels to predict for. Default is [1, 2, 3, 4, 5, 6].
        - km_mean: boolean indicating whether to calculate the mean of the upper and lower estimates. Default is True.

        Returns:
        - A pandas DataFrame with predictions for each severity level at each time point in `t`.
        """
        p = {}
        for s in severities:
            if s == severities[0]:
                predictions = self.list_models[self.list_models['s>'] == s].iloc[0].turnbull_estimator.predict(t)
                p[f's_{s}_up'], p[f's_{s}_down'] = predictions['NPMLE_estimate_upper'], predictions['NPMLE_estimate_lower']
            else:
                current_predictions = self.list_models[self.list_models['s>'] == s].iloc[0].turnbull_estimator.predict(t)
                previous_predictions = self.list_models[self.list_models['s>'] == s-1].iloc[0].turnbull_estimator.predict(t)
                difference = current_predictions - previous_predictions
                p[f's_{s}_up'], p[f's_{s}_down'] = difference['NPMLE_estimate_upper'], difference['NPMLE_estimate_lower']
        p = pd.DataFrame(p)
        p.index = t
        
        if km_mean:
            # Calculate the mean of the upper and lower estimates for each severity level
            for i in severities:
                prefix = f's_{i}'
                up_col = f'{prefix}_up'
                down_col = f'{prefix}_down'
                p[prefix] = p[[up_col, down_col]].mean(axis=1)
                p.drop(columns=[up_col, down_col], inplace=True)

        return p

    def plot(self, t=np.arange(0, 50)):
        """
        Plots the predictions for given time points.

        Parameters:
        - t: numpy array of time points for which to plot predictions. Default is np.arange(0, 50).
        """
        predictions = self.predict(t)
        for column in predictions.columns:
            plt.plot(t, predictions[column], label=column)
        plt.legend()
        plt.show()
